import numpy as np

from estimators import Estimator


# Define a class lD that inherits from the Estimator class
class lD(Estimator):
    def _one_estimation_correlation(
            i0, i1, i2, i3,
            Y0, Y1, Y2, Y3,
            d0, d1, d2, d3,
            dNeigh,
            onlyOne
    ):
        # Find the indices where the values in d0 at i0
        #   are less than or equal to dNeigh
        neigh0 = np.where(d0[i0, :] <= dNeigh)[1]
        # Compute the mean of Y0 across the selected neighbors
        Y0b = np.mean(Y0[:, neigh0], axis=1)

        # Similar operations are performed for Y1, Y2, and Y3
        neigh1 = np.where(d1[i1, :] <= dNeigh)[1]
        Y1b = np.mean(Y1[:, neigh1], axis=1)

        neigh2 = np.where(d2[i2, :] <= dNeigh)[1]
        Y2b = np.mean(Y2[:, neigh2], axis=1)

        neigh3 = np.where(d3[i3, :] <= dNeigh)[1]
        Y3b = np.mean(Y3[:, neigh3], axis=1)

        # Call the _outside and _corNoisy methods from
        #   the Estimator class to compute a result
        estimation = Estimator._outside(
            Estimator._corNoisy(Y0b, Y1b, Y2b, Y3b)
            )

        # Check if any of the neighborhoods have only one element
        if (
            (len(neigh0) == 1)
            or (len(neigh1) == 1)
            or (len(neigh2) == 1)
            or (len(neigh3) == 1)
        ):
            onlyOne += 1  # Increment the counter

        return estimation, onlyOne

    # Define a static method lD_estimator
    @staticmethod
    def estimator_correlation(Y0: np.ndarray[float],
                              Y1: np.ndarray[float],
                              Y2: np.ndarray[float],
                              Y3: np.ndarray[float],
                              d0: np.ndarray[float],
                              d1: np.ndarray[float],
                              d2: np.ndarray[float],
                              d3: np.ndarray[float],
                              B: int = 10,
                              dNeigh: float = 1) -> dict:
        """Compute inter-region correlation between voxel time-series of
            two regions using lD estimator

        Parameters
        ----------
        Y0 : first region

        Y1 : second region

        Y2 and Y3 : regions which are uncorrelated to each and
            to the first two regions

        B : number of iterations to compute correlation

        d0 : distances between vectors of matrix coordinates
            of the first region

        d1 : distances between vectors of matrix coordinates
            of the second region

        d2 : distances between vectors of matrix coordinates
            of the third region

        d3 : distances between vectors of matrix coordinates
            of the fourth region

        dNeigh : distance threshold for neighbourhoods (p in the manuscript).
            Default 1. Unit: voxels. For l* family methods,
        defines the neighbourhood used for aggregation
        - voxels at a distance closer than equal to dNeigh will be
        considered part of the neighbourhood.

        each region contains:
        * a matrix 'Y' of voxel time series, dimension T x V
        * a matrix 'coords' of spatial coordinates of each voxel,
            dimension 3 x V
        * a vector 'w' of voxel weights, dimension 1 x V, each between 0 and 1.

        Returns
        -------
        estimation : dict
            Correlation estimate between the two regions of interest.
        """
        onlyOne = 0  # Initialize a counter variable
        estimations_array = np.zeros(B)  # Create an array to store the results

        # Perform B iterations
        for b in range(B):
            # Randomly choose an index from the range of Y0 shape
            i0 = np.random.choice(range(Y0.shape[1]), 1)
            # Similar operations are performed for i1, i2, and i3
            i1 = np.random.choice(range(Y1.shape[1]), 1)
            i2 = np.random.choice(range(Y2.shape[1]), 1)
            i3 = np.random.choice(range(Y3.shape[1]), 1)

            # Calculate the correlation using these indexs
            estimation = lD._one_estimation_correlation(
                                i0, i1, i2, i3,
                                Y0, Y1, Y2, Y3,
                                d0, d1, d2, d3,
                                dNeigh,
                                onlyOne
                            )
            estimations_array[b], onlyOne = estimation

        # Compute the final estimate, the sum of NaN values, and the count
        #   of neighborhoods with only one element
        est = max(min(
                    np.mean(estimations_array[~np.isnan(estimations_array)]), 1
                    ), -1)

        sumNA = np.sum(np.isnan(estimations_array))

        # Return a dictionary containing the estimate, sum of NaN values,
        #   and count of neighborhoods with only one element
        return {"est": est, "sumNA": sumNA, "onlyOne": onlyOne}
