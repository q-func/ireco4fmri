# Import necessary libraries
from nibabel.affines import apply_affine
import numpy as np
from dataclasses import dataclass
from nilearn import datasets
import nibabel as nib
np.random.seed(22)


# Define a class named Regions
class Regions:
    # Constructor for the Regions class
    def __init__(self, nifti_4D_file: str, atlas=datasets.fetch_atlas_aal()):
        """The atlas used by default is the AAL dataset.
        """
        self.atlas = atlas
        self.regions: 'Region' = self._construct_regions(nifti_4D_file)

    # Private method to construct regions based on the provided NIfTI 4D file
    def _construct_regions(self, nifti_4D_file: str) -> 'Region':
        # Create a Region object to store the regions
        regions: 'Region' = Region()

        # Load the 4D NIfTI image and its data
        nifti_img = nib.load(nifti_4D_file)
        nifti_data = nifti_img.get_fdata()
        nifti_affine = nifti_img.affine

        # Load the atlas image and its data
        atlas_filename = self.atlas["maps"]
        atlas_img = nib.load(atlas_filename)
        atlas_data = atlas_img.get_fdata()
        inv_atlas_affine = np.linalg.inv(atlas_img.affine)
        labels = []

        # Iterate through each voxel in the 4D nifti data
        for x in range(nifti_data.shape[0]):
            for y in range(nifti_data.shape[1]):
                for z in range(nifti_data.shape[2]):
                    # Get the MNI coordinates of the voxel
                    mni_coords = apply_affine(nifti_affine, [x, y, z])

                    # Convert voxel coordinates to atlas coordinates
                    voxel_coords_in_atlas = apply_affine(
                        inv_atlas_affine,
                        mni_coords)

                    # Find the closest voxel in the AAL atlas
                    closest_voxel_idx = tuple(
                        np.round(
                            voxel_coords_in_atlas).astype(int))

                    # Get the AAL region label for the closest voxel in the
                    # atlas
                    if 0 <= closest_voxel_idx[0] < atlas_data.shape[0] and \
                        0 <= closest_voxel_idx[1] < atlas_data.shape[1] and \
                            0 <= closest_voxel_idx[2] < atlas_data.shape[2]:

                        region_label = atlas_data[closest_voxel_idx]
                        labels.append(region_label)
                        # Create a Voxel object and add it to the
                        # corresponding region in the regions dictionary
                        voxel: Voxel = Voxel(
                            time_series=nifti_data[x, y, z],
                            mni_coords=mni_coords,
                            nifti_img_coords=coordinates(x, y, z),
                            atlas_coords=coordinates(
                                closest_voxel_idx[0],
                                closest_voxel_idx[1],
                                closest_voxel_idx[2]),
                            region_label=region_label
                        )
                        regions = self._add_to_dict(
                            regions, region_label, voxel)

        # Return the regions dictionary with all the regions
        return regions.next_region

    # Private method to add a voxel to the corresponding region in
    # the regions linked list
    def _add_to_dict(self,
                     brain_regions: 'Region',
                     region_label: int,
                     voxel: 'Voxel'):
        regions = brain_regions
        while regions is not None:
            if region_label == regions.label:
                if regions.voxels is None:
                    regions.voxels = voxel
                else:
                    voxel.next_voxel = regions.voxels
                    regions.voxels = voxel
                return brain_regions
            else:
                regions = regions.next_region
        new_region = Region(label=region_label, voxels=voxel)
        new_region.next_region = brain_regions.next_region
        brain_regions.next_region = new_region
        return brain_regions


# Define a Region class to represent a region in the brain 
class Region(object):
    def __init__(self, label: int = None, voxels: 'Voxel' = None,
                 next_region: 'Region' = None) -> None:
        self.label: int = label
        self.voxels: 'Voxel' = voxels
        self.next_region: 'Region' = next_region

    # Method to search for voxels of a specific region and return their data
    def search_voxels_of_a_region(self, label) -> dict:
        """Search the label of a specific region in the linked list of regions
        and return the data of the voxels in the region if the label is found
        """
        while self is not None:
            if self.label == label:
                return self.voxels.voxels_data_in_arrays()
            self = self.next_region
        raise ValueError(f"Region with label {label} not found")

    # Generator function to yield all region labels in the regions linked list
    def regions_labels(self):
        while self is not None:
            yield self.label
            self = self.next_region


# Define a dataclass named coordinates to represent voxel coordinates
@dataclass
class coordinates(object):
    x: int
    y: int
    z: int


# Define a class named Voxel to represent a voxel in the brain
class Voxel:
    def __init__(self, time_series: list[float],
                 mni_coords: coordinates, nifti_img_coords: coordinates,
                 atlas_coords: coordinates, region_label: int,
                 next_voxel: 'Voxel' = None) -> None:
        self.time_series = time_series
        self.mni_coords = mni_coords
        self.nifti_img_coords = nifti_img_coords  # indexs of the voxel
        # in the nifti image
        self.atlas_coords = atlas_coords  # coordinates of the
        # closest voxel in the AAL atlas to this voxel
        self.region_label = region_label  # identifier for
        # the region of that voxel
        self.next_voxel = next_voxel  # pointer to another
        # voxel in the same region

    # Method to collect voxel data and coordinates in arrays for a region
    # sins the voxels are stored in a linked list
    # we only need the head of the list to extract the data
    def voxels_data_in_arrays(self) -> np.ndarray:
        voxels_time_series: list[list[float]] = []
        voxel_coords: list[coordinates] = []
        while self is not None:
            voxels_time_series.append(list(self.time_series))
            atlas_coords = self.atlas_coords
            voxel_coords.append(
                [atlas_coords.x, atlas_coords.y, atlas_coords.z])
            self = self.next_voxel
        return {"ts": np.transpose(np.array(voxels_time_series)),
                "coords": np.transpose(np.array(voxel_coords))}


def print_regions_names_and_labels():
    atlas = datasets.fetch_atlas_aal()
    for region_name, region_label in zip(atlas['labels'], atlas['indices']):
        print(f"The region name is {region_name} : \
and its label is {region_label}")


def get_regions_names_and_labels():
    atlas = datasets.fetch_atlas_aal()
    atlas_indices = [int(index) for index in atlas['indices']]
    return dict(zip(atlas_indices, atlas['labels']))


def get_one_region_name_or_label(identifier_region):
    atlas = datasets.fetch_atlas_aal()
    atlas_indices = [int(index) for index in atlas['indices']]
    dict_indices_names = dict(zip(atlas_indices, atlas['labels']))
    dict_names_indices = dict(zip(atlas['labels'], atlas_indices))
    if (identifier_region in dict_indices_names.keys()):
        return dict_indices_names[identifier_region]
    elif (identifier_region in dict_names_indices.keys()):
        return dict_names_indices[identifier_region]
    else:
        raise ValueError(f"The region {identifier_region} \
is not in the atlas")
