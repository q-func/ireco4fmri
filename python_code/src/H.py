import numpy as np
from scipy.cluster.hierarchy import cut_tree, linkage
from scipy.spatial.distance import squareform

from estimators import Estimator


# Define a class H that inherits from the Estimator class
class H(Estimator):
    @staticmethod
    def estimator_correlation(index_regions_1: int,
                              index_regions_2: int,
                              clustering: dict) -> float:
        # get the first region data
        reg_clust1 = clustering["avg_ts_clusters_ROI_" + str(index_regions_1)]

        # get the second region
        reg_clust2 = clustering["avg_ts_clusters_ROI_" + str(index_regions_2)]

        # compute the inter-correlations
        inter_clust12 = Estimator.calcul_corr(reg_clust1.T, reg_clust2.T)

        return np.mean(inter_clust12)

    @staticmethod
    def get_Udist(reg: np.ndarray[float]) -> np.ndarray[float]:
        # Compute the correlation matrix
        corr_matrix = np.corrcoef(reg, rowvar=False)

        # Make the correlation matrix symmetric
        corr_matrix = (corr_matrix + corr_matrix.T) / 2.0

        # Compute the U-score distance matrix
        distUreg = np.sqrt(2 * (1 - corr_matrix))

        # Set the diagonal elements to zero
        np.fill_diagonal(distUreg, 0)

        # Convert the matrix to a condensed distance vector
        distUreg_condensed = squareform(distUreg)

        return distUreg_condensed

    @staticmethod
    def hcut_reg_maxdistU(hreg: np.ndarray[float],
                          Udist: np.ndarray[float]) -> np.ndarray[float]:
        # Choose h as the maximum distance between U-scores
        h_maxUdist = np.max(Udist)

        # Cut the hierarchical tree at height h
        cut_hreg = cut_tree(hreg, height=h_maxUdist)

        return cut_hreg

    @staticmethod
    def hclust_allregs(PATH: str,
                       regions: list,
                       minvox: int = 10,
                       tsfiles: str = "/dwt_ts_ROI_") -> dict:
        # Create the clustering dictionary
        clustering = {}

        # loop over regions that have more than minvox voxels
        for i in regions:
            # get the region i data
            regt = np.loadtxt(PATH + tsfiles + str(i) + ".txt")

            # ensures region reg contains more than minvox voxels
            if regt.shape[0] >= minvox:
                reg = regt.T  # reg is an n x p matrix

                # hierarchical clustering

                # compute the U-score distance matrix
                Udistreg = H.get_Udist(reg)

                # Hierarchical clustering of Udistreg with Ward's criterion
                hreg = linkage(Udistreg, method="ward")

                # cut the dendogram
                hcut = H.hcut_reg_maxdistU(hreg, Udistreg)

                # Compute the average time series of each cluster
                unique_clusters = np.unique(hcut)
                NeighAvg_h = np.zeros((len(unique_clusters), reg.T.shape[1]))

                for j, cluster in enumerate(unique_clusters):
                    cluster_indices = np.where(hcut == cluster)[0]
                    cluster_time_series = reg.T[cluster_indices]
                    average_time_series = np.mean(cluster_time_series, axis=0)
                    NeighAvg_h[j] = average_time_series

                # Store the average time series in the dictionary
                #   (each line corresponds to a cluster)
                clustering["avg_ts_clusters_ROI_" + str(i)] = NeighAvg_h

        return clustering
