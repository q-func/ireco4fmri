import numpy as np
import pywt
import math
import warnings
warnings.filterwarnings("ignore")

# LA8 filter coefficients
dec_lo = [-0.0757657147893567, -0.0296355276459604, 0.497618667632563,
          0.803738751805386, 0.297857795605605, -0.0992195435769564,
          -0.0126039672622638, 0.0322231006040782]
dec_hi = [0.0322231006040782, 0.0126039672622638, -0.0992195435769564,
          -0.297857795605605, 0.803738751805386, -0.497618667632563,
          -0.0296355276459604, 0.0757657147893567]
rec_lo = [0.0322231006040782, -0.0126039672622638, -0.0992195435769564,
          0.297857795605605, 0.803738751805386, 0.497618667632563,
          -0.0296355276459604, -0.0757657147893567]
rec_hi = [0.0757657147893567, -0.0296355276459604, -0.497618667632563,
          0.803738751805386, -0.297857795605605, -0.0992195435769564,
          0.0126039672622638, 0.0322231006040782]

# LA8 wavelet construction
wavelet_name = pywt.Wavelet("la8", filter_bank=[dec_lo, dec_hi, rec_lo,
                                                rec_hi])

# Length of the filter
longueur = 8


# n=length of time series
# N=length of filter
def compute_nj(n, N):
    nj = []
    nj.append(n)
    J = 2
    while (nj[J - 2] - N + 1) >= 1:
        nj.append(math.ceil((nj[J - 2] - N + 1) / 2))
        J += 1
    J -= 2
    nj = nj[1:]  # Slice from index 1 to remove the first element in Python
    return {'nj': nj, 'J': J}


# Number of coefficients to remove at the beginning and the end
# depending on the scale
def delete_coeffs(level):
    if level == 1:
        n_start = 3
        n_end = 4
    elif level == 2:
        n_start = 9
        n_end = 12
    elif level == 3:
        n_start = 21
        n_end = 28
    elif level == 4:
        n_start = 45
        n_end = 60
    return n_start, n_end


def tranforme_en_ondelettes(voxel_time_series, level=None):
    # Checking and adjusting the decomposition level
    max_level = compute_nj(len(voxel_time_series), longueur)["J"] - 1
    if level is None and max_level >= 4:
        level = 4
    elif level is None and max_level < 4:
        level = max_level
    elif level is not None and max_level > level:
        raise ValueError(f"Could not do the transformation to wavelet "
                         f"coefficients because the level {level} is too high")

    # Calculating wavelet coefficients
    coeffs = pywt.swt(voxel_time_series, wavelet_name, level=level,
                      start_level=0, axis=-1, trim_approx=True, norm=True)

    # Retrieving detail coefficients from the last level
    detail_coeffs = coeffs[1]

    # Coefficients to remove
    n_start, n_end = delete_coeffs(level)

    # Removing coefficients at the beginning and the end whose total is
    # n = (2**level-1)*(longueur-1)
    detail_coeffs = np.delete(detail_coeffs, [i for i in range(0, n_start)])
    detail_coeffs = np.delete(detail_coeffs,
                              [i for i in range(len(detail_coeffs) - n_end,
                                                len(detail_coeffs))])

    return detail_coeffs
