import numpy as np
from scipy.spatial.distance import cdist

from AC import AC
from ACc import ACc
from CA import CA
from D import D
from estimators import Estimator
from lCA import lCA
from lD import lD
from lR import lR
from lRD import lRD
from R import R
from RD import RD


def estCorr(
    data: list,
    meth: list[str] = ["CA",
                       "AC",
                       "ACc",
                       "lCA",
                       "R",
                       "lR",
                       "D",
                       "lD",
                       "RD",
                       "lRD"],
    param: dict = {"B": 10, "dNeigh": 1, "dRepl": 1},
    thresh: float = 0.5,
) -> dict:
    """Compute inter-region correlations between voxel time-series
        using various Estimator

    Parameters
    ----------
    data  : 4-dimensional list.
        The first two elements are regions for which we
        want to compute correlations. The last two elements are regions which
        are uncorrelated to the first two. The last two elements are only
        necessary for Estimator D, lD, RD, lRD. With V the number of voxels
        in the region and T the number of
        samples in time, each region contains:
        * a matrix 'Y' of voxel time series, dimension T x V
        * a matrix 'coords' of spatial coordinates of each voxel,
            dimension 3 x V
        * a vector 'w' of voxel weights, dimension 1 x V, each between 0 and 1.
        This allows to select only certain voxels for the analysis.

    meth  : string
        A string, specifying the estimation method.
        One of "CA","AC","ACc","lCA","R","lR","D","lD","RD","lRD".

    param : dict, default : {'B': 500, 'dNeigh': 1, 'dRepl': 1}
        a dictionary containing estimator parameters (B, dNeigh, dRepl)
            as keys where :
        * B: number of boostrap replicates. Default 500.
        * dNeigh: distance threshold for neighbourhoods (p in the manuscript).
        Default 1. Unit: voxels. For l* family methods,
        defines the neighbourhood used for aggregation
        - voxels at a distance closer than equal to dNeigh will be
        considered part of the neighbourhood.
        * dRepl: For method "R" and "RD", the exact distance
            between replicate voxels. Default 1. Unit: voxels. This forces
        replicate voxels to be distinct from the originally sampled voxel.
        For methods "lR" and "lRD", the offset distance between neighbourhoods
        of replicates (set to dRepl+2*dNeigh).
        This prevents neighbourhoods of replicates from overlapping

    thresh : float, default = 0.5
        a scalar between 0 and 1. This threshold allows to only select voxels
        whose 'w' weight parameter (see above) is
        above the threshold.

    Returns
    -------
    estimation : dict
        Correlation estimate between the two regions of interest.
    """

    if not isinstance(data, list):
        raise ValueError("data needs to be a list")
    elif len(data) != 4:
        raise ValueError("data needs to be a list of length 4")

    # Get indexs of the data
    def where_data_index(i):
        return np.where(data[i]["w"] > thresh)[0]

    i0, i1, i2, i3 = (
        where_data_index(0),
        where_data_index(1),
        where_data_index(2),
        where_data_index(3),
    )

    # Get the data of the 4 regions

    # data of the first region
    Y0 = data[0]["Y"][:, i0]
    coordsR0 = data[0]["coords"][:, i0]

    # data of the second region
    Y1 = data[1]["Y"][:, i1]
    coordsR1 = data[1]["coords"][:, i1]

    # data of the third region
    Y2 = data[2]["Y"][:, i2]
    coordsR2 = data[2]["coords"][:, i2]

    # data of the fourth region
    Y3 = data[3]["Y"][:, i3]
    coordsR3 = data[3]["coords"][:, i3]

    # distances between vectors of matrix coordinates of each region
    def fct_cdist_matrix(matr):
        return cdist(matr.T, matr.T, metric="chebyshev")

    d0, d1, d2, d3 = (
        fct_cdist_matrix(coordsR0),
        fct_cdist_matrix(coordsR1),
        fct_cdist_matrix(coordsR2),
        fct_cdist_matrix(coordsR3),
    )

    # Associate each estimator to his function
    dict_estimator_functions = {
        "CA": CA.estimator_correlation,
        "AC": AC.estimator_correlation,
        "ACc": ACc.estimator_correlation,
        "lCA": lCA.estimator_correlation,
        "R": R.estimator_correlation,
        "lR": lR.estimator_correlation,
        "D": D.estimator_correlation,
        "lD": lD.estimator_correlation,
        "RD": RD.estimator_correlation,
        "lRD": lRD.estimator_correlation,
    }

    # test if the estimator is known by the function
    if meth not in dict_estimator_functions.keys():
        raise ValueError("No estimator of this name")

    # Associate each estimator to his signature
    estimators_functions_signatures = Estimator.construct_sig_estimtors(
        Y0=Y0, Y1=Y1, Y2=Y2, Y3=Y3, param=param, d0=d0, d1=d1, d2=d2, d3=d3
    )

    # Get the signature of the estimator used
    signature_estimator_function = estimators_functions_signatures[meth]

    dict_estimation = dict_estimator_functions[meth](
        *signature_estimator_function
    )  # a dictionnary containing the estimation
    est, sumNA, onlyOne = (
        dict_estimation["est"],
        dict_estimation["sumNA"],
        dict_estimation["onlyOne"],
    )  # extract useful variables
    estimation = {
        "est": est,
        "sumNA": sumNA,
        "onlyOne": onlyOne,
    }  # a dictionnary of useful variables
    return estimation
