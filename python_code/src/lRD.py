import numpy as np

from estimators import Estimator


# Define a class lRD that inherits from the Estimator class
class lRD(Estimator):
    def _one_estimation_correlation(
            i0, i1, i2, i3,
            Y0, Y1, Y2, Y3,
            d0, d1, d2, d3,
            dNeigh,
            repli0, repli1,
            onlyOne
    ):

        # Find the neighbors of i0 within a distance of dNeigh
        #   in the d0 matrix
        neigh0 = np.where(d0[i0, :] <= dNeigh)[1]
        # Calculate the mean of the corresponding columns of Y0
        #   using the obtained neighbors
        Y0b = np.mean(Y0[:, neigh0], axis=1)

        # Find the neighbors of repli0 within a distance of dNeigh
        #   in the d0 matrix
        neighRepl0 = np.where(d0[repli0, :] <= dNeigh)[1]
        # Calculate the mean of the corresponding columns of Y0 using
        #   the obtained neighbors
        Y0replb = np.mean(Y0[:, neighRepl0], axis=1)

        # Repeat the same process for Y1, Y2, and Y3 arrays
        neigh1 = np.where(d1[i1, :] <= dNeigh)[1]
        Y1b = np.mean(Y1[:, neigh1], axis=1)
        neighRepl1 = np.where(d1[repli1, :] <= dNeigh)[1]
        Y1replb = np.mean(Y1[:, neighRepl1], axis=1)

        neigh2 = np.where(d2[i2, :] <= dNeigh)[1]
        Y2b = np.mean(Y2[:, neigh2], axis=1)

        neigh3 = np.where(d3[i3, :] <= dNeigh)[1]
        Y3b = np.mean(Y3[:, neigh3], axis=1)

        # Calculate the result by passing the mean values of Y0b, Y0replb,
        #   Y1b, Y1replb, Y2b, and Y3b to the _corDoubleNoise method
        #   from the Estimator class
        estimation = Estimator._outside(
            Estimator._corDoubleNoise(Y0b, Y0replb, Y1b, Y1replb, Y2b, Y3b)
        )

        # Increment the onlyOne counter if any of the neighborhoods
        #   have a length of 1
        if (
            (len(neigh0) == 1)
            or (len(neigh1) == 1)
            or (len(neighRepl0) == 1)
            or (len(neighRepl1) == 1)
            or (len(neigh2) == 1)
            or (len(neigh3) == 1)
        ):
            onlyOne += 1

        return estimation, onlyOne

    @staticmethod
    def estimator_correlation(Y0: np.ndarray[float], Y1: np.ndarray[float],
                              Y2: np.ndarray[float],
                              Y3: np.ndarray[float],
                              d0: np.ndarray[float],
                              d1: np.ndarray[float],
                              d2: np.ndarray[float],
                              d3: np.ndarray[float],
                              B: int = 10,
                              dRepl: float = 1,
                              dNeigh: float = 1) -> dict:
        """Compute inter-region correlation between voxel time-series
            of two regions using lRD estimator

        Parameters
        ----------
        Y0 : first region

        Y1 : second region

        Y2 and Y3 : regions which are uncorrelated to each other
            and to the first two regions

        B : number of iterations to compute correlation

        d0 : distances between vectors of matrix coordinates
            of the first region

        d1 : distances between vectors of matrix coordinates
            of the second region

        d2 : distances between vectors of matrix coordinates
            of the third region

        d3 : distances between vectors of matrix coordinates
            of the fourth region

        dRepl: For method "R" and "RD", the exact distance between
            replicate voxels. Default 1. Unit: voxels. This forces
        replicate voxels to be distinct from the originally sampled voxel.
        For methods "lR" and "lRD", the offset distance between neighbourhoods
            of replicates (set to dRepl+2*dNeigh).
        This prevents neighbourhoods of replicates from overlapping

        dNeigh : distance threshold for neighbourhoods (p in the manuscript).
            Default 1. Unit: voxels. For l* family methods,
        defines the neighbourhood used for aggregation
            - voxels at a distance closer than equal to dNeigh will be
             considered part of the neighbourhood.

        each region contains:
        * a matrix 'Y' of voxel time series, dimension T x V
        * a matrix 'coords' of spatial coordinates of each voxel,
            dimension 3 x V
        * a vector 'w' of voxel weights, dimension 1 x V, each between 0 and 1.

        Returns
        -------
        estimation : dict
            Correlation estimate between the two regions of interest.
        """
        onlyOne = 0
        estimations_array = np.zeros(B)
        for b in range(B):
            # Randomly select an index i0 from the range of Y0.shape[1]
            i0 = np.random.choice(range(Y0.shape[1]), 1)
            # Repeat the same process for Y1, Y2, and Y3 arrays
            i1 = np.random.choice(range(Y1.shape[1]), 1)
            i2 = np.random.choice(range(Y2.shape[1]), 1)
            i3 = np.random.choice(range(Y3.shape[1]), 1)

            # Randomly select an index repli0 from the indices where d0[i0, :]
            #   equals (dRepl + 2 * dNeigh)
            where_d0 = np.where(d0[i0, :] == (dRepl + 2 * dNeigh))[1]
            repli0 = np.random.choice(where_d0, 1)

            # Repeat the same process for Y1, Y2, and Y3 arrays
            where_d1 = np.where(d1[i1, :] == (dRepl + 2 * dNeigh))[1]
            repli1 = np.random.choice(where_d1, 1)

            # Calculate the result by passing the mean values of Y0b, Y0replb,
            #   Y1b, Y1replb, Y2b, and Y3b to the _corDoubleNoise method
            #   from the Estimator class
            estimation, onlyOne = lRD._one_estimation_correlation(
                                        i0, i1, i2, i3,
                                        Y0, Y1, Y2, Y3,
                                        d0, d1, d2, d3,
                                        dNeigh,
                                        repli0, repli1,
                                        onlyOne
            )
            estimations_array[b] = estimation

        # Calculate the estimate by taking the mean of the result array,
        #   excluding NaN values,
        #   and ensuring it is within the range [-1, 1] using
        #   max(min(value, 1), -1)
        est = max(min(
                    np.mean(estimations_array[~np.isnan(estimations_array)]), 1
                    ), -1)

        # Calculate the sum of NaN values in the result array
        sumNA = np.sum(np.isnan(estimations_array))

        # Return a dictionary containing the estimate, sum of NaN values,
        #   and the onlyOne counter
        return {"est": est, "sumNA": sumNA, "onlyOne": onlyOne}
