import numpy as np

from estimators import Estimator


# Define a class AC that inherits from the Estimator class
class AC(Estimator):
    @staticmethod
    def estimator_correlation(Y0: np.ndarray[float],
                              Y1: np.ndarray[float]) -> dict:
        """Compute inter-region correlation between voxel time-series
            of two regions using AC estimator

        Parameters
        ----------
        Y0 : first region
        Y1 : second region

        each region contains:
        * a matrix 'Y' of voxel time series, dimension T x V
        * a matrix 'coords' of spatial coordinates of each voxel,
            dimension 3 x V
        * a vector 'w' of voxel weights, dimension 1 x V, each between 0 and 1.

        Returns
        -------
        estimation : dict
            Correlation estimate between the two regions of interest.
        """

        # Compute the average column correlation between Y0 and Y1 arrays
        # using the columns_corr method from the Estimator class
        avg_column_corr = np.mean(Estimator.columns_corr(Y0, Y1))

        # Compute the final estimate by taking the average column correlation
        # and ensuring it is within the range [-1, 1] using
        # max(min(value, 1), -1)
        est = max(min(avg_column_corr, 1), -1)

        # Return a dictionary containing the estimate, sum of NaN values
        # (which is always 0 in this case),
        # and a value of 0 for the onlyOne key
        return {"est": est, "sumNA": 0, "onlyOne": 0}
