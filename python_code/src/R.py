import numpy as np

from estimators import Estimator


# Define a class R that inherits from the Estimator class
class R(Estimator):
    def _one_estimation_correlation(
        i0, i1,
        Y0, Y1,
        repli0, repli1,
    ):
        # Extract the corresponding columns from Y0 and Y1 using
        # the selected indices
        Y0b = Y0[:, i0]
        Y0replb = Y0[:, repli0]
        Y1b = Y1[:, i1]
        Y1replb = Y1[:, repli1]

        # Calculate the result by passing the extracted columns to the
        # _corLocNoisy method from the Estimator class
        estimation = Estimator._outside(
            Estimator._corLocNoisy(Y0b, Y0replb, Y1b, Y1replb)
        )

        return estimation

    @staticmethod
    def estimator_correlation(Y0: np.ndarray[float], Y1: np.ndarray[float],
                              d0: np.ndarray[float],
                              d1: np.ndarray[float],
                              B: int = 1,
                              dRepl: float = 1) -> dict:
        """Compute inter-region correlation between voxel time-series
            of two regions using R estimator

        Parameters
        ----------
        Y0 : first region

        Y1 : second region

        B : number of iterations to compute correlation

        d0 : distances between vectors of matrix coordinates
            of the first region

        d1 : distances between vectors of matrix coordinates
            of the second region

        dRepl: For method "R" and "RD", the exact distance
            between replicate voxels. Default 1. Unit: voxels. This forces
            replicate voxels to be distinct from the originally sampled voxel.
        For methods "lR" and "lRD", the offset distance between neighbourhoods
            of replicates (set to dRepl+2*dNeigh).
        This prevents neighbourhoods of replicates from overlapping

        each region contains:
        * a matrix 'Y' of voxel time series, dimension T x V
        * a matrix 'coords' of spatial coordinates of each voxel,
            dimension 3 x V
        * a vector 'w' of voxel weights, dimension 1 x V, each between 0 and 1.

        Returns
        -------
        estimation : dict
            Correlation estimate between the two regions of interest.
        """
        onlyOne = 0
        estimations_array = np.zeros(B)
        for b in range(B):
            # Randomly select an index i0 from the range of Y0.shape[1]
            i0 = np.random.choice(range(Y0.shape[1]), 1)
            # Randomly select an index i1 from the range of Y1.shape[1]
            i1 = np.random.choice(range(Y1.shape[1]), 1)

            # While there are no repli0 and repli1 indices in
            # d0 and d1 matrices with value dRepl,
            # continue selecting new random indices
            # and increment the onlyOne counter
            while (np.sum(d0[i0, :] == dRepl) == 0) or (
                np.sum(d1[i1, :] == dRepl) == 0
            ):
                onlyOne += 1
                i0 = np.random.choice(range(Y0.shape[1]), 1)
                i1 = np.random.choice(range(Y1.shape[1]), 1)

            # Randomly select a repli0 index from the indices where d0[i0, :]
            # equals dRepl
            repli0 = np.random.choice(np.where(d0[i0, :] == dRepl)[1], 1)
            # Randomly select a repli1 index from the indices where d1[i1, :]
            # equals dRepl
            repli1 = np.random.choice(np.where(d1[i1, :] == dRepl)[1], 1)

            # Calculate the result by passing the extracted columns to the
            # _corLocNoisy method from the Estimator class
            estimation = R._one_estimation_correlation(
                        i0, i1,
                        Y0, Y1,
                        repli0, repli1,
            )
            estimations_array[b] = estimation

        # Calculate the estimate by taking the mean of the result array,
        # excluding NaN values,
        # and ensuring it is within the range [-1, 1] using
        # max(min(value, 1), -1)
        est = max(min(np.mean(
            estimations_array[~np.isnan(estimations_array)]), 1), -1)

        # Calculate the sum of NaN values in the result array
        sumNA = np.sum(np.isnan(estimations_array))

        # Return a dictionary containing the estimate, sum of NaN values,
        # and the onlyOne counter
        return {"est": est, "sumNA": sumNA, "onlyOne": onlyOne}
