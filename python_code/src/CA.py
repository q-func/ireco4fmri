import numpy as np

from estimators import Estimator


# Define a class CA that inherits from the Estimator class
class CA(Estimator):
    @staticmethod
    def estimator_correlation(Y0: np.ndarray[float],
                              Y1: np.ndarray[float]) -> dict:
        """Compute inter-region correlation between voxel time-series
            of two regions using CA estimator

        Parameters
        ----------
        Y0 : first region
        Y1 : second region

        each region contains:
        * a matrix 'Y' of voxel time series, dimension T x V
        * a matrix 'coords' of spatial coordinates of each voxel,
            dimension 3 x V
        * a vector 'w' of voxel weights, dimension 1 x V, each between 0 and 1.

        Returns
        -------
        estimation : dict
            Correlation estimate between the two regions of interest.
        """

        # Compute the correlation coefficient between the mean values of
        # Y0 and Y1 arrays
        mean_Y0 = np.mean(Y0, axis=1)
        mean_Y1 = np.mean(Y1, axis=1)
        correlation_coefficient = np.corrcoef(mean_Y0, mean_Y1)[0, 1]
        est = max(min(correlation_coefficient, 1), -1)

        # Return a dictionary containing the estimate, sum of NaN values
        # (which is always 0 in this case),
        # and a value of 0 for the onlyOne key
        return {"est": est, "sumNA": 0, "onlyOne": 0}
