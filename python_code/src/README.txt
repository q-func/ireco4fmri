Files organization in the 'src' directory:

'estimators.py': This file contains an abstract class,
which serves as the parent class for all estimators.
It includes common methods shared by all estimators.

'lCA.py', 'CA.py', 'ACc.py', 'AC.py', 'lCA.py', 'D.py',
'lD.py', 'lRD.py', 'lR.py', 'R.py', 'RD.py': These files
represent the different estimators that inherit from the
'estimators.py' file.

'estCorr.py': This file takes data from four regions and
the name of the estimator and computes correlations between them.

'ondelettes.py': This file performs the transformation of time series
into a wavelet representation.

'process_nifti_image_4D.py': This file is used to divide a 4D nifti
image of the brain into regions and obtain the time series and
coordinates of the voxels inside them.

'compute_corr.py': This file takes a 4D nifti image as input,
along with identifiers of the regions for which correlation needs to
be computed (the default regions used in 'fit_regions' are 'Temporal_Sup_R',
'Temporal_Mid_R', 'Rolandic_Oper_R', 'Heschl_R'). The result is the
correlation matrix between these regions.
