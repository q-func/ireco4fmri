import numpy as np

from estimators import Estimator


# Define a class lR that inherits from the Estimator class
class lR(Estimator):
    def _one_estimation_correlation(
            i0, i1,
            Y0, Y1,
            d0, d1,
            dNeigh,
            repli0, repli1,
            onlyOne
    ):
        # Find the neighbors of i0 within a distance of dNeigh
        #   in the d0 matrix
        neigh0 = np.where(d0[i0, :] <= dNeigh)[1]
        # Calculate the mean of the corresponding columns of Y0 using the
        #   obtained neighbors
        Y0b = np.mean(Y0[:, neigh0], axis=1)

        # Find the neighbors of repli0 within a distance of dNeigh
        #   in the d0 matrix
        neighRepl0 = np.where(d0[repli0, :] <= dNeigh)[1]
        # Calculate the mean of the corresponding columns of Y0
        #   using the obtained neighbors
        Y0replb = np.mean(Y0[:, neighRepl0], axis=1)

        # Find the neighbors of i1 within a distance of dNeigh
        #   in the d1 matrix
        neigh1 = np.where(d1[i1, :] <= dNeigh)[1]
        # Calculate the mean of the corresponding columns of Y1
        #   using the obtained neighbors
        Y1b = np.mean(Y1[:, neigh1], axis=1)

        # Find the neighbors of repli1 within a distance of dNeigh
        #   in the d1 matrix
        neighRepl1 = np.where(d1[repli1, :] <= dNeigh)[1]
        # Calculate the mean of the corresponding columns of Y1
        #   using the obtained neighbors
        Y1replb = np.mean(Y1[:, neighRepl1], axis=1)

        # Calculate the result by passing the mean values of Y0b, Y0replb,
        #   Y1b, and Y1replb to the _corLocNoisy method
        #   from the Estimator class
        estimation = Estimator._outside(
            Estimator._corLocNoisy(Y0b, Y0replb, Y1b, Y1replb)
        )

        # Increment the onlyOne counter if any of the neighborhoods
        #   have a length of 1
        if (
            (len(neigh0) == 1)
            or (len(neigh1) == 1)
            or (len(neighRepl0) == 1)
            or (len(neighRepl1) == 1)
        ):
            onlyOne += 1

        return estimation, onlyOne

    @staticmethod
    def estimator_correlation(Y0: np.ndarray[float], Y1: np.ndarray[float],
                              d0: np.ndarray[float],
                              d1: np.ndarray[float],
                              B: int = 5,
                              dRepl: float = 1,
                              dNeigh: float = 1) -> dict:
        """Compute inter-region correlation between voxel time-series
            of two regions using lR estimator

        Parameters
        ----------
        Y0 : first region

        Y1 : second region

        B : number of iterations to compute correlation

        d0 : distances between vectors of matrix coordinates
             of the first region

        d1 : distances between vectors of matrix coordinates
            of the second region

        dRepl: For method "R" and "RD", the exact distance between
            replicate voxels. Default 1. Unit: voxels.
        This forces replicate voxels to be distinct from
            the originally sampled voxel.
        For methods "lR" and "lRD", the offset distance between neighbourhoods
            of replicates (set to dRepl+2*dNeigh).
        This prevents neighbourhoods of replicates from overlapping

        dNeigh : distance threshold for neighbourhoods (p in the manuscript).
            Default 1. Unit: voxels. For l* family methods,
        defines the neighbourhood used for aggregation
            - voxels at a distance closer than equal to dNeigh will be
            considered part of the neighbourhood.

        each region contains:
        * a matrix 'Y' of voxel time series, dimension T x V
        * a matrix 'coords' of spatial coordinates of each voxel,
            dimension 3 x V
        * a vector 'w' of voxel weights, dimension 1 x V, each between 0 and 1.

        Returns
        -------
        estimation : dict
            Correlation estimate between the two regions of interest.
        """
        onlyOne = 0
        estimations_array = np.zeros(B)
        for b in range(B):
            # Randomly select an index i0 from the range of Y0.shape[1]
            i0 = np.random.choice(range(Y0.shape[1]), 1)

            # Randomly select an index repli0 from the indices where d0[i0, :]
            #   equals (dRepl + 2 * dNeigh)
            where_d0 = np.where(d0[i0, :] == (dRepl + 2 * dNeigh))[1]
            repli0 = np.random.choice(where_d0, 1)

            # Randomly select an index i1 from the range of Y1.shape[1]
            i1 = np.random.choice(range(Y1.shape[1]), 1)

            # Randomly select an index repli1 from the indices where d1[i1, :]
            #   equals (dRepl + 2 * dNeigh)
            where_d1 = np.where(d1[i1, :] == (dRepl + 2 * dNeigh))[1]
            repli1 = np.random.choice(where_d1, 1)

            # Calculate the result by passing the mean values of Y0b, Y0replb,
            #   Y1b, and Y1replb to the _corLocNoisy method
            #   from the Estimator class
            estimation, onlyOne = lR._one_estimation_correlation(
                    i0, i1,
                    Y0, Y1,
                    d0, d1,
                    dNeigh,
                    repli0, repli1,
                    onlyOne
            )
            estimations_array[b] = estimation

        est = max(min(np.mean(
            estimations_array[~np.isnan(estimations_array)]), 1), -1)
        sumNA = np.sum(np.isnan(estimations_array))
        return {"est": est, "sumNA": sumNA, "onlyOne": onlyOne}
