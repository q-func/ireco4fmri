import numpy as np
from scipy.spatial.distance import cdist
from nilearn import datasets
from itertools import combinations
from typing import Union
from ondelettes import tranforme_en_ondelettes

from process_nifti_image_4D import Regions, get_one_region_name_or_label
from AC import AC
from ACc import ACc
from CA import CA
from D import D
from estimators import Estimator
from lCA import lCA
from lD import lD
from lR import lR
from lRD import lRD
from R import R
from RD import RD


def fit_2_regions(
        estimator: str = "lCA",
        nifti_file_img_4D: str =
        datasets.fetch_development_fmri(n_subjects=1).func[0],
        label_region_1: Union[str, int] = 8112,
        label_region_2: Union[str, int] = 8202) -> float:
    """Compute correlation between two regions identified by label_region_1
        and label_region_2.

    Parameters:
    ----------
    estimator (str, optional): the estimator to use to compute correlation.
        Defaults to "lCA".

    nifti_file_img_4D: The absolute path to a 4D nifti image from which
        to extract regions and compute correlation between them.
        Defaults to datasets.fetch_development_fmri(n_subjects=1).func[0]
        imported from nilearn which could be used as an example.

    label_region_1 (float, optional): the label of the first region we could
        use the number that identifies a region as well as the name of the
        region. Defaults to 8112.0 (region `Temporal_Sup_R`)

    label_region_2 (float, optional): the label of the second region we could
        use the number that identifies a region as well as the name of the
        region. Defaults to 8202.0 (region `Temporal_Mid_R`)

    Returns:
        correlation : the correlation between the two regions.

    !!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!
    NB : The atlas AAL is used to divide the nifti_file_img_4D into regions
    to see regions names and the numbers that identifies them call
    the function print_regions_names_and_labels from process_nifti_image_4D:
    code :
        from process_nifti_image_4D import print_regions_names_and_labels
        print_regions_names_and_labels(nifti_file_img_4D)

    To see the number that identifies a region or the inverse call the function
    et_one_region_name_or_label
    """
    # if the regions are identified with the name of the region
    # get the number that identifies that name of that region
    if (type(label_region_1) is str):
        label_region_1 = get_one_region_name_or_label(
                    label_region_1)
        label_region_2 = get_one_region_name_or_label(
                    label_region_2)

    # construct a Regions object that contains the regions
    # and inside them the voxels and the coordinates of the voxels
    # and their time series
    regions_in_nifti_img_4D = Regions(nifti_file_img_4D)

    # Recuperate data of the region identified by label_region_1
    data_label_1 = regions_in_nifti_img_4D.regions.\
        search_voxels_of_a_region(label_region_1)
    Y0 = data_label_1['ts']
    Y0 = np.transpose(np.array([tranforme_en_ondelettes(Y0[:, i])
                                for i in range(Y0.shape[1])]))
    coordsR0 = data_label_1['coords']

    # Recuperate data of the region identified by label_region_2
    data_label_2 = regions_in_nifti_img_4D.regions.\
        search_voxels_of_a_region(label_region_2)
    Y1 = data_label_2['ts']
    Y1 = np.transpose(np.array([tranforme_en_ondelettes(Y1[:, i])
                                for i in range(Y1.shape[1])]))
    coordsR1 = data_label_2['coords']

    # Recuperate data of the region 3 and 4
    # Those 2 regions are uncorrelated to each other
    # and to the other regions
    data_2_3 = regions_in_nifti_img_4D.regions.\
        search_voxels_of_a_region(0)
    Y_2_3 = data_2_3['ts']
    coordsR2_3 = data_2_3['coords']
    Y2 = Y_2_3[:, :100]
    Y2 = np.transpose(np.array([tranforme_en_ondelettes(Y2[:, i])
                                for i in range(Y2.shape[1])]))
    coordsR2 = coordsR2_3[:, :100]
    Y3 = Y_2_3[:, -100:]
    Y3 = np.transpose(np.array([tranforme_en_ondelettes(Y3[:, i])
                                for i in range(Y3.shape[1])]))
    coordsR3 = coordsR2_3[:, -100:]

    # distances between vectors of matrix coordinates of each region
    def fct_cdist_matrix(matr):
        return cdist(matr.T, matr.T, metric="chebyshev")

    d0, d1, d2, d3 = (
        fct_cdist_matrix(coordsR0),
        fct_cdist_matrix(coordsR1),
        fct_cdist_matrix(coordsR2),
        fct_cdist_matrix(coordsR3),
    )

    # Associate each estimator to his function
    dict_estimator_functions = {
        "CA": CA.estimator_correlation,
        "AC": AC.estimator_correlation,
        "ACc": ACc.estimator_correlation,
        "lCA": lCA.estimator_correlation,
        "R": R.estimator_correlation,
        "lR": lR.estimator_correlation,
        "D": D.estimator_correlation,
        "lD": lD.estimator_correlation,
        "RD": RD.estimator_correlation,
        "lRD": lRD.estimator_correlation,
    }

    # test if the estimator is known by the function
    if estimator not in dict_estimator_functions.keys():
        raise ValueError("No estimator of this name")

    # Associate each estimator to his signature
    param: dict = {"B": 10, "dNeigh": 1, "dRepl": 1}
    estimators_functions_signatures = Estimator.construct_sig_estimtors(
        Y0=Y0, Y1=Y1, Y2=Y2, Y3=Y3, param=param, d0=d0, d1=d1, d2=d2, d3=d3
    )

    # Get the signature of the estimator used
    signature_estimator_function = estimators_functions_signatures[estimator]

    dict_estimation = dict_estimator_functions[estimator](
        *signature_estimator_function
    )  # a dictionnary containing the estimation

    correlation = dict_estimation["est"]
    return correlation


def fit_regions(
        estimator: str = "lCA",
        nifti_file_img_4D: str =
        datasets.fetch_development_fmri(n_subjects=1).func[0],
        regions_identifiers: list = [8112.0, 8202.0, 2332.0, 8102.0]):
    """Compute the matrix of correlation between regions identified
        by regions_identifiers

    Parameters:
    ----------
    estimator (str, optional): the estimator to use to compute correlation.
        Defaults to "lCA".

    nifti_file_img_4D: The absolute path to a 4D nifti image from which
        to extract regions and compute correlation between them.
        Defaults to datasets.fetch_development_fmri(n_subjects=1).func[0]
        imported from nilearn which could be used as an example.

    regions_identifiers (list, optional): the list of regions that we want
        to compute correlation between them

    Returns:
        matrix_of_correlation : the matrix of correlation between the
        two regions at index i and j in the matrix_of_correlation there
        is the correlation between regions_identifiers[i] and
        regions_identifiers[j]

    NB : [8112.0, 8202.0, 2332.0, 8102.0] identifies regions
    ['Temporal_Sup_R', 'Temporal_Mid_R', 'Rolandic_Oper_R', 'Heschl_R']
    We could also have passed the list
    ['Temporal_Sup_R', 'Temporal_Mid_R', 'Rolandic_Oper_R', 'Heschl_R']
    to the parameter regions_identifiers.

    !!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!
    NB : The atlas AAL is used to divide the nifti_file_img_4D into regions.
    To see regions names and the numbers that identifies them call
    the function print_regions_names_and_labels from process_nifti_image_4D:
    code :
        from process_nifti_image_4D import print_regions_names_and_labels
        print_regions_names_and_labels(nifti_file_img_4D)

    To see the number that identifies a region or the inverse call the function
    get_one_region_name_or_label
    """
    # if the regions are identified with the name of the region
    # get the number that identifies that name of that region
    if (type(regions_identifiers[0]) is str):
        regions_labels = [get_one_region_name_or_label(
                    identifier_region) for identifier_region
                          in regions_identifiers]
    else:
        regions_labels = regions_identifiers

    # A dictionnary containing the index of the labels of the regions
    # in regions_labels
    regions_labels_indexs = {label: i
                             for i, label in enumerate(regions_labels)}
    number_of_regions = len(regions_labels)
    print(f"Regions labels: {regions_labels}")
    matrix_of_correlation = np.zeros((number_of_regions, number_of_regions))

    # for each pair of regions compute the correlation between them
    combinations_of_regions = list(combinations(regions_labels, 2))
    for label_region_1, label_region_2 in combinations_of_regions:
        correlation_between_2_regions = fit_2_regions(
            estimator,
            nifti_file_img_4D,
            label_region_1,
            label_region_2)

        matrix_of_correlation[
                    regions_labels_indexs[label_region_1],
                    regions_labels_indexs[label_region_2]
                    ] = correlation_between_2_regions

        matrix_of_correlation[
                    regions_labels_indexs[label_region_2],
                    regions_labels_indexs[label_region_1]
                    ] = correlation_between_2_regions

    regions_names = [get_one_region_name_or_label(label)
                     for label in regions_labels]
    print(f"matrix_of_correlation of regions {regions_names} :\n \
{matrix_of_correlation}")
    return matrix_of_correlation


if __name__ == "__main__":
    fit_regions()
