import numpy as np

from estimators import Estimator


# Define a class ACc that inherits from the Estimator class
class ACc(Estimator):
    @staticmethod
    def estimator_correlation(Y0: np.ndarray[float],
                              Y1: np.ndarray[float]) -> dict:
        """Compute inter-region correlation between voxel time-series
            of two regions using ACc estimator

        Parameters
        ----------
        Y0 : first region
        Y1 : second region

        each region contains:
        * a matrix 'Y' of voxel time series, dimension T x V
        * a matrix 'coords' of spatial coordinates of each voxel,
            dimension 3 x V
        * a vector 'w' of voxel weights, dimension 1 x V, each between 0 and 1.

        Returns
        -------
        estimation : dict
            Correlation estimate between the two regions of interest.
        """

        # Compute the correlation coefficient between the mean values
        #  of Y0 and Y1 arrays
        corr_coef = np.corrcoef(np.mean(Y0, axis=1), np.mean(Y1, axis=1))[0, 1]

        # Compute the square root of the product of the average column
        # correlations of Y0 and Y1 arrays
        avg_column_corr = np.sqrt(
            np.mean(Estimator.columns_corr(Y0, Y0))
            * np.mean(Estimator.columns_corr(Y1, Y1))
        )

        # Compute the final estimate by multiplying the correlation
        # coefficient by the average column correlation
        # and ensure it is within the range [-1, 1] using
        # max(min(value, 1), -1)
        est = max(min(avg_column_corr * corr_coef, 1), -1)

        # Return a dictionary containing the estimate, sum of NaN values
        # (which is always 0 in this case),
        # and a value of 0 for the onlyOne key
        return {"est": est, "sumNA": 0, "onlyOne": 0}
