import numpy as np

from estimators import Estimator


# Define the RD class that inherits from estimators
class RD(Estimator):
    def _one_estimation_correlation(
        i0, i1, i2, i3,
        Y0, Y1, Y2, Y3,
        repli0, repli1,
    ):
        # Select the voxel time series for the selected voxels
        Y0b = Y0[:, i0]
        Y0replb = Y0[:, repli0]
        Y1b = Y1[:, i1]
        Y1replb = Y1[:, repli1]
        Y2b = Y2[:, i2]
        Y3b = Y3[:, i3]

        # Compute the correlation using the Estimator module
        estimation = Estimator._outside(
            Estimator._corDoubleNoise(Y0b, Y0replb, Y1b, Y1replb, Y2b, Y3b)
        )
        return estimation

    @staticmethod
    def estimator_correlation(Y0: np.ndarray[float],
                              Y1: np.ndarray[float],
                              Y2: np.ndarray[float],
                              Y3: np.ndarray[float],
                              d0: np.ndarray[float],
                              d1: np.ndarray[float],
                              B: int = 10,
                              dRepl: float = 1) -> dict:
        """Compute inter-region correlation between voxel time-series of
            two regions using RD estimator

        Parameters:
        Y0, Y1, Y2, Y3: The regions used for correlation computation.
        B: Number of iterations to compute correlation.
        d0, d1: Distances between vectors of matrix coordinates of
            the first and second regions.
        dRepl: Distance between replicate voxels or offset distance between
            neighborhoods of replicates.

        Each region contains:
        - A matrix 'Y' of voxel time series, dimension T x V.
        - A matrix 'coords' of spatial coordinates of each voxel,
            dimension 3 x V.
        - A vector 'w' of voxel weights, dimension 1 x V, each between 0 and 1.

        Returns:
        A dictionary containing the correlation estimate between
            the two regions, along with other statistics.
        """

        onlyOne = 0
        estimations_array = np.zeros(B)  # Initialize an array to
        # store correlation results

        # Perform B iterations
        for b in range(B):
            # Randomly select voxels from Y0 and Y1
            i0 = np.random.choice(range(Y0.shape[1]), 1)
            i1 = np.random.choice(range(Y1.shape[1]), 1)

            # Continue selecting until at least one voxel has a distance
            #   of dRepl in both d0 and d1
            while (np.sum(d0[i0, :] == dRepl) == 0) or (
                np.sum(d1[i1, :] == dRepl) == 0
            ):
                onlyOne += 1
                i0 = np.random.choice(range(Y0.shape[1]), 1)
                i1 = np.random.choice(range(Y1.shape[1]), 1)

            # Select a replicate voxel based on dRepl distance
            repli0 = np.random.choice(np.where(d0[i0, :] == dRepl)[1], 1)
            repli1 = np.random.choice(np.where(d1[i1, :] == dRepl)[1], 1)

            # Randomly select voxels from Y2 and Y3
            i2 = np.random.choice(range(Y2.shape[1]), 1)
            i3 = np.random.choice(range(Y3.shape[1]), 1)

            # Compute the correlation using the Estimator module
            estimation = RD._one_estimation_correlation(
                i0, i1, i2, i3,
                Y0, Y1, Y2, Y3,
                repli0, repli1,
            )
            estimations_array[b] = estimation

        # Calculate the correlation estimate as the mean of estimations_array,
        # ensuring it is between -1 and 1
        est = max(min(np.nanmean(estimations_array), 1), -1)
        sumNA = np.sum(np.isnan(estimations_array))
        return {"est": est, "sumNA": sumNA, "onlyOne": onlyOne}
