from abc import ABC, abstractmethod

import numpy as np


class Estimator(ABC):
    @staticmethod
    @abstractmethod
    def estimator_correlation(*args, **kwargs) -> dict:
        """Compute inter-region correlation between voxel time-series
            of two regions

        This function should be overrided in every estimator
        """

    @staticmethod
    def _corNoisy(
        Y1: np.ndarray[float],
        Y2: np.ndarray[float],
        Y3: np.ndarray[float],
        Y4: np.ndarray[float]
    ) -> float:
        """
        Calculate the correlation between noisy data sets.

        Parameters
        ----------
        Y1 : ndarray
            First set of data.
        Y2 : ndarray
            Second set of data.
        Y3 : ndarray
            Third set of data.
        Y4 : ndarray
            Fourth set of data.

        Returns
        -------
        result : ndarray
            Correlation matrix between the noisy data sets.

        Note: If any of the covariance values or diagonal elements
            of the covariance matrices
        are negative, the function returns NaN (Not a Number).
        """

        # Reshape the input arrays if needed
        Y1 = Y1.reshape((Y1.shape[0], 1)) if Y1.shape == (Y1.shape[0],) else Y1
        Y2 = Y2.reshape((Y2.shape[0], 1)) if Y2.shape == (Y2.shape[0],) else Y2
        Y3 = Y3.reshape((Y3.shape[0], 1)) if Y3.shape == (Y3.shape[0],) else Y3
        Y4 = Y4.reshape((Y4.shape[0], 1)) if Y4.shape == (Y4.shape[0],) else Y4

        # Calculate the covariance matrices for the differences
        covariance_1 = (
            np.cov((Y1 - Y3).T) + np.cov((Y1 - Y4).T) - np.cov((Y3 - Y4).T)
        ) / 2
        covariance_2 = (
            np.cov((Y2 - Y3).T) + np.cov((Y2 - Y4).T) - np.cov((Y3 - Y4).T)
        ) / 2

        # Extract the diagonal elements or treat as scalar if necessary
        v1 = (
            np.diag(covariance_1)
            if (type(covariance_1) == np.ndarray)
            else covariance_1
        )
        v2 = (
            np.diag(covariance_2)
            if (type(covariance_2) == np.ndarray)
            else covariance_2
        )

        # Check if any covariance values or diagonal elements are negative
        if np.any(np.concatenate((np.array([v1]), np.array([v2]))) < 0):
            return np.nan

        # Create matrices for multiplication
        m_gauche = v1[:, np.newaxis] if (type(v1) == np.ndarray) else v1
        m_droit = v2[np.newaxis, :] if (type(v2) == np.ndarray) else v2

        # Compute the multiplication of the matrices
        v = np.dot(m_gauche, m_droit)

        # Calculate the covariance matrix and divide by the square root of v
        m_covariance = np.cov(Y1 - Y3, Y2 - Y4, rowvar=False)
        result = m_covariance[: Y1.shape[1], Y1.shape[1]:] / np.sqrt(v)

        return result

    @staticmethod
    def columns_corr(Y0, Y1):
        """Calculate correlations between columns of 2 matrices

        Parameters
        -----------
        Y0 : ndarray
            first matrix
        Y1 : ndarray
            second matrix

        Returns
        ---------
        correlations : ndarray
            matrix of correlations between columns of the 2 matrices
        """
        # initialize the matrix of correlation between the two matrices
        correlations = np.zeros((Y0.shape[1], Y1.shape[1]))
        for i in range(Y0.shape[1]):
            for j in range(Y1.shape[1]):
                # correlation between 2 columns
                correlations[i, j] = np.corrcoef(Y0[:, i], Y1[:, j])[0, 1]
        return correlations


    @staticmethod
    def _corLocNoisy(
        Y1: np.ndarray[float],
        Y1repl: np.ndarray[float],
        Y2: np.ndarray[float],
        Y2repl: np.ndarray[float]
    ) -> float:
        """Calculate the normalized correlation between two sets
            of data with replacements.

        Parameters
        ----------
        Y1 : ndarray
            First set of data.
        Y1repl : ndarray
            Replacements for the first set of data.
        Y2 : ndarray
            Second set of data.
        Y2repl : ndarray
            Replacements for the second set of data.

        Returns
        -------
        correlation : float
            Normalized correlation between the two sets of data.

        Note: If the correlation coefficient between any of the pairs of data
        is negative, the function returns NaN (Not a Number).
        """

        # Calculate the correlation coefficient between Y1 and Y1repl
        c1 = np.corrcoef(Y1.T, Y1repl.T)[0, 1]

        # Calculate the correlation coefficient between Y2 and Y2repl
        c2 = np.corrcoef(Y2.T, Y2repl.T)[0, 1]

        # Check if either correlation coefficient is negative
        if c1 < 0 or c2 < 0:
            return np.nan
        else:
            # Combine Y1 and Y1repl using column-wise binding
            combined_Y1 = np.column_stack((Y1, Y1repl))

            # Combine Y2 and Y2repl using column-wise binding
            combined_Y2 = np.column_stack((Y2, Y2repl))

            # Calculate the correlation between combined_Y1 and combined_Y2
            correlation_intermediate = Estimator.columns_corr(
                combined_Y1,
                combined_Y2
                )

            # Compute the mean correlation
            correlation = np.mean(correlation_intermediate)

            # Normalize the correlation by dividing by the square root
            # of (c1 * c2)
            return correlation / np.sqrt(c1 * c2)

    @staticmethod
    def _corDoubleNoise(
        Y1: np.ndarray[float],
        Y1repl: np.ndarray[float],
        Y2: np.ndarray[float],
        Y2repl: np.ndarray[float],
        Y3: np.ndarray[float],
        Y4: np.ndarray[float]
    ) -> float:
        """
        Calculate the normalized correlation between two sets of data.

        Parameters
        ----------
        Y1 : ndarray
            First set of data.
        Y1repl : ndarray
            Replacements for the first set of data.
        Y2 : ndarray
            Second set of data.
        Y2repl : ndarray
            Replacements for the second set of data.
        Y3 : ndarray
            Third set of data.
        Y4 : ndarray
            Fourth set of data.

        Returns
        -------
        correlation : float
            Normalized correlation between the two sets of double-noisy data.

        Note: If any of the correlation coefficients is negative or NaN,
        the function returns NaN.
        """

        # Calculate the numerator by computing the mean correlation
        # of the noisy data
        num = np.mean(
            Estimator._corNoisy(
                np.column_stack((Y1, Y1repl)),
                np.column_stack((Y2, Y2repl)),
                np.column_stack((Y3, Y3)),
                np.column_stack((Y4, Y4)),
            )
        )

        # Calculate the correlation coefficients for the noisy data pairs
        c1 = Estimator._corNoisy(Y1, Y1repl, Y3, Y4)
        c2 = Estimator._corNoisy(Y2, Y2repl, Y3, Y4)

        # Check if any of the correlation coefficients are NaN or negative
        if np.isnan(c1) or np.isnan(c2) or np.any(np.array([c1, c2]) < 0):
            return np.nan
        else:
            # Calculate the denominator as the square root of (c1 * c2)
            denom = np.sqrt(c1 * c2)

            # Calculate the normalized correlation
            return num / denom

    @staticmethod
    def _outside(est: float) -> float:
        """
        Replace values _outside the range [-1, 1] with NaN.

        Parameters
        ----------
        est : ndarray
            Array of estimated values.

        Returns
        -------
        corrected_est : ndarray
            Array of estimated values with values _outside the range
            replaced by NaN.
        """

        # Check if values in 'est' are _outside the range [-1, 1]
        condition = np.logical_or(est < -1, est > 1)

        # Replace values _outside the range with NaN
        corrected_est = np.where(condition, np.nan, est)

        return corrected_est

    @staticmethod
    def construct_sig_estimtors(Y0: np.ndarray[float],
                                Y1: np.ndarray[float],
                                Y2: np.ndarray[float],
                                Y3: np.ndarray[float],
                                param: dict,
                                d0: np.ndarray[float],
                                d1: np.ndarray[float],
                                d2: np.ndarray[float],
                                d3: np.ndarray[float]) -> dict:
        """Construct signature of functions

        Parameters
        -----------
        Y0 : ndarray
            data of the first region

        Y1 : ndarray
            data of the second region

        Y2 : ndarray
            data of the third region

        Y3 : ndarray
            data of the fourth region

        NB : the third and fourth region are uncorrelated between each other
            and the other regions

        param : dict, default : {'B': 500, 'dNeigh': 1, 'dRepl': 1}
            a dictionary containing estimator parameters (B, dNeigh, dRepl)
            as keys where :
            * B: number of boostrap replicates. Default 500.
            * dNeigh: distance threshold for neighbourhoods
                (p in the manuscript). Default 1. Unit: voxels.
                For l* family methods,
                defines the neighbourhood used for aggregation
                - voxels at a distance closer than equal to dNeigh will be
                considered part of the neighbourhood.
            * dRepl: For method "R" and "RD", the exact distance
                between replicate voxels. Default 1. Unit: voxels. This forces
                replicate voxels to be distinct from the originally
                sampled voxel.
            For methods "lR" and "lRD", the offset distance between
                neighbourhoods of replicates (set to dRepl+2*dNeigh).
            This prevents neighbourhoods of replicates from overlapping

        d0 : ndarray
        distances between vectors of matrix coordinates of the first region

        d1 : ndarray
            distances between vectors of matrix coordinates
            of the second region

        d2 : ndarray
            distances between vectors of matrix coordinates of the third region

        d3 : ndarray
            distances between vectors of matrix coordinates
                of the fourth region


        Returns
        ---------
        signatures : dict
            a dictionnary containing for each estimator his signature
        """
        B = param["B"]
        dNeigh = param["dNeigh"]
        dRepl = param["dRepl"]
        sig_CA = (Y0, Y1)
        sig_AC = (Y0, Y1)
        sig_ACc = (Y0, Y1)
        sig_lCA = (Y0, Y1, d0, d1, B, dNeigh)
        sig_R = (Y0, Y1, d0, d1, B, dRepl)
        sig_lR = (Y0, Y1, d0, d1, B, dRepl, dNeigh)
        sig_D = (Y0, Y1, Y2, Y3, B)
        sig_lD = (Y0, Y1, Y2, Y3, d0, d1, d2, d3, B, dNeigh)
        sig_RD = (Y0, Y1, Y2, Y3, d0, d1, B, dRepl)
        sig_lRD = (Y0, Y1, Y2, Y3, d0, d1, d2, d3, B, dRepl, dNeigh)
        signatures = {
            "CA": sig_CA,
            "AC": sig_AC,
            "ACc": sig_ACc,
            "lCA": sig_lCA,
            "R": sig_R,
            "lR": sig_lR,
            "D": sig_D,
            "lD": sig_lD,
            "RD": sig_RD,
            "lRD": sig_lRD,
        }
        return signatures
