import numpy as np

from estimators import Estimator


# Define a class D that inherits from the Estimator class
class D(Estimator):
    @staticmethod
    def _one_estimation_of_correlation(Y0, Y1, Y2, Y3, i0, i1, i2, i3):
        # Get the columns corresponding to the randomly chosen
        #   indices for each Y array
        Y0b = Y0[:, i0]
        Y1b = Y1[:, i1]
        Y2b = Y2[:, i2]
        Y3b = Y3[:, i3]

        # Compute the result using the _outside and _corNoisy methods
        #   from the Estimator class
        estimation = Estimator._outside(
            Estimator._corNoisy(Y0b, Y1b, Y2b, Y3b)
            )
        return estimation

    @staticmethod
    def estimator_correlation(Y0: np.ndarray[float],
                              Y1: np.ndarray[float],
                              Y2: np.ndarray[float],
                              Y3: np.ndarray[float],
                              B: int = 10) -> dict:
        """Compute inter-region correlation between voxel time-series
            of two regions using D estimator

        Parameters
        ----------

        Y0 : first region

        Y1 : second region

        Y2 and Y3 : regions which are uncorrelated to each other
            and to the first two regions

        B : number of iterations to compute correlation

        each region contains:
        * a matrix 'Y' of voxel time series, dimension T x V
        * a matrix 'coords' of spatial coordinates of each voxel,
             dimension 3 x V
        * a vector 'w' of voxel weights, dimension 1 x V, each between 0 and 1.

        Returns
        -------
        estimation : dict
            Correlation estimate between the two regions of interest.
        """

        estimations_array = np.zeros(B)  # Create an array to store the results

        # Perform B iterations
        for b in range(B):
            # Randomly choose an index from the range of Y0 shape
            #   and get the corresponding column
            i0 = np.random.choice(range(Y0.shape[1]), 1)
            i1 = np.random.choice(range(Y1.shape[1]), 1)
            i2 = np.random.choice(range(Y2.shape[1]), 1)
            i3 = np.random.choice(range(Y3.shape[1]), 1)
            # Calculate the estimation from these indexs
            estimation = D._one_estimation_of_correlation(
                Y0=Y0, Y1=Y1, Y2=Y2, Y3=Y3, i0=i0, i1=i1, i2=i2, i3=i3)
            estimations_array[b] = estimation
        # Compute the final estimate, the sum of NaN values
        #   (which is always 0 in this case),
        # and return a dictionary containing the estimate, sum of NaN values,
        #   and a value of 0 for onlyOne
        est = max(min(
                    np.mean(estimations_array[~np.isnan(estimations_array)]), 1
                    ), -1)
        sumNA = np.sum(np.isnan(estimations_array))
        return {"est": est, "sumNA": sumNA, "onlyOne": 0}
