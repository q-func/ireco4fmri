import numpy as np

from estimators import Estimator


# Define a class lCA that inherits from the Estimator class
class lCA(Estimator):
    def _one_estimation_of_correlation(Y0, Y1, d0, d1, i0, i1, dNeigh,
                                       onlyOne=0):
        # Find the indices where the values in d0 at i0
        #   are less than or equal to dNeigh
        neigh0 = np.where(d0[i0, :] <= dNeigh)[1]
        # Compute the mean of Y0 across the selected neighbors
        Y0b = np.mean(Y0[:, neigh0], axis=1)

        # Similar operations are performed for Y1
        neigh1 = np.where(d1[i1, :] <= dNeigh)[1]
        Y1b = np.mean(Y1[:, neigh1], axis=1)

        # Compute the correlation coefficient between Y0b and Y1b
        estimation = np.corrcoef(Y0b, Y1b)[0, 1]

        # Check if either neighborhood has only one element
        if len(neigh0) == 1 or len(neigh1) == 1:
            onlyOne += 1  # Increment the counter
        return estimation, onlyOne

    # Define a static method lCA_estimator
    @staticmethod
    def estimator_correlation(Y0: np.ndarray[float],
                              Y1: np.ndarray[float],
                              d0: np.ndarray[float],
                              d1: np.ndarray[float],
                              B: int = 10,
                              dNeigh: float = 1) -> dict:
        """Compute inter-region correlation between voxel time-series
            of two regions using lCA estimator

        Parameters
        ----------
        Y0 : first region

        Y1 : second region

        B : number of iterations to compute correlation

        d0 : distances between vectors of matrix coordinates
            of the first region

        d1 : distances between vectors of matrix coordinates
            of the second region

        dNeigh : distance threshold for neighbourhoods (p in the manuscript).
            Default 1. Unit: voxels. For l* family methods,
            defines the neighbourhood used for aggregation
            - voxels at a distance closer than equal to dNeigh will be
            considered part of the neighbourhood.

        each region contains:
        * a matrix 'Y' of voxel time series, dimension T x V
        * a matrix 'coords' of spatial coordinates of each voxel,
            dimension 3 x V
        * a vector 'w' of voxel weights, dimension 1 x V, each between 0 and 1.

        Returns
        -------
        estimation : dict
            Correlation estimate between the two regions of interest.
        """
        onlyOne = 0  # Initialize a counter variable
        estimations_array = np.zeros(B)  # Create an array to store the results

        # Perform B iterations
        for b in range(B):
            # Randomly choose an index from the range of Y0 shape
            i0 = np.random.choice(range(Y0.shape[1]), 1)
            # Randomly choose an index from the range of Y1 shape
            i1 = np.random.choice(range(Y1.shape[1]), 1)

            # Compute the correlation coefficient between Y0b and Y1b
            estimation, onlyOne = lCA._one_estimation_of_correlation(
                                                Y0=Y0, Y1=Y1,
                                                d0=d0, d1=d1,
                                                i0=i0, i1=i1,
                                                dNeigh=dNeigh,
                                                onlyOne=onlyOne)
            estimations_array[b] = estimation

        #  Compute the final estimate as the mean of estimations_array,
        #   and return a dictionary containing the estimate,
        #   sum of NaN values (which is always 0 in this case),
        #   and the count of neighborhoods with only one element
        est = max(min(np.mean(estimations_array), 1), -1)
        return {"est": est, "sumNA": 0, "onlyOne": onlyOne}
