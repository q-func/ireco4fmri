import time


def time_decorator(func):
    """A decorator that measures time taken by a function
    to be executed
    """
    def wrapper(*args, **kwargs):
        start_time = time.time()
        _ = func(*args, **kwargs)
        end_time = time.time()
        execution_time_in_seconds = end_time - start_time
        return execution_time_in_seconds
    return wrapper
