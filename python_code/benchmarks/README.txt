Files organization in the 'benchmarks' directory:

"time_decorator.py": This file contains a function that takes another
function as input and returns a modified version of that function.
The modified function performs the same task as the input function
but also measures the time taken for its execution.
The purpose of this file is to provide a decorator that can be applied
to any function to measure its execution time.

"time_benchmark.py": This file is used to benchmark and compare the
execution times of different estimators.  The purpose of this file
is to compare the performance of different estimators in terms of
their execution time.