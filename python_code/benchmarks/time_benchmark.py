import numpy as np
import sys
import os
from time_decorator import time_decorator

sys.path.append(os.path.abspath(os.path.join(
    os.getcwd(), 'python_matrices_of_corr/')))
sys.path.append(os.path.abspath(os.path.join(
    os.pardir, 'python_matrices_of_corr/')))
from one_estimator_matrix_of_correlation import \
    calculate_matrix_of_correlation_using_one_estimator


def print_in_ascending_order(map_estimators_time_execution:
                             list[tuple[str, float]])\
                                -> None:
    """Print the time taken by estimator to compute correlation
    in an ascending order
    """
    print("\nExecution time for estimators in an ascending order :\n")
    sorted_map = sorted(map_estimators_time_execution,
                        key=lambda tuple_est_time: tuple_est_time[1])
    for estimator, execution_time_in_seconds in sorted_map:
        print(f"Execution time of the estimator {estimator} is  : \
                {execution_time_in_seconds}")
    print()


def one_simulation_time_execution(estimators: tuple[str])\
        -> dict[str, float]:
    """Return in a dictionnary containing for every estimator the time
    he takes to compute correlation in a simulation
    """
    estimators_time_execution: dict[str, float] = dict()
    for estimator in estimators:
        execution_time_in_seconds = time_decorator(
            calculate_matrix_of_correlation_using_one_estimator
            )(
            estimator=estimator)
        estimators_time_execution[estimator] = execution_time_in_seconds
    return estimators_time_execution


def initialize_dict(estimators: tuple[str])\
        -> dict[str, list[float]]:
    """Initialize a dictionnary of lists of estimators """
    map_estimators = {}
    for estimator in estimators:
        map_estimators[estimator] = []
    return map_estimators


def measure_time_execution(number_of_simulations: int = 1)\
        -> list[tuple[str, float]]:
    """Compute for every the average of the times he takes to be executed

    Parameters:
    -----------
    number_of_simulations (int): The number of simulations we want

    Returns:
    ----------
    estimators_time_execution : a list of tuples with as first value
        an estimator and as second value\
        his mean execution times
    """

    ESTIMATORS: tuple[str] = ("CA",
                              "AC",
                              "ACc",
                              "lCA",
                              "R",
                              "lR",
                              "D",
                              "lD",
                              "RD",
                              "lRD")

    map_estimator_time_execution: dict[str, list[float]] = initialize_dict(
        ESTIMATORS)

    for _ in range(number_of_simulations):
        map_one_simulation = one_simulation_time_execution(ESTIMATORS)
        for estimator, time_execution in map_one_simulation.items():
            map_estimator_time_execution[estimator].append(time_execution)

    map_of_averages: dict[str, float] = {
        estimator: np.mean(np.array(map_estimator_time_execution[estimator]))
        for estimator in ESTIMATORS
        }

    estimators_time_execution = [
        (estimator, map_of_averages[estimator])
        for estimator in ESTIMATORS
        ]

    print_in_ascending_order(estimators_time_execution)

    return estimators_time_execution


if __name__ == "__main__":
    _ = measure_time_execution()
