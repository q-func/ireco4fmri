import os
import sys
from itertools import combinations

import numpy as np
from processing_1 import (get_all_files_in_folder_matching_number,
                          process_region)

sys.path.append(os.path.abspath(os.path.join(os.getcwd(), 'src/')))
sys.path.append(os.path.abspath(os.path.join(os.pardir, 'src/')))
from estCorr import estCorr
np.random.seed(42)


def processing() -> tuple[list, str]:
    """A function that do preprocessing in order to get the path
    to the folder that contains relevant data and the data of the region 3
    and 4 that are used within some estimators

    Returns:
    ----------
    data: data of regions 3 and 4

    folder_path: path to the folder that contains data
    """
    # The folder that contains files from which we want to get data
    if ("one_rat_data" in os.listdir(os.getcwd())):
        folder_path = os.getcwd() + "/one_rat_data"
    elif ("one_rat_data" in os.listdir(os.pardir)):
        folder_path = os.pardir + "/one_rat_data/"
    else:
        raise ValueError("There is no folder with the name one_rat_data\
                        that contains data")

    # path to the data of regions 3 and 4 that would be used in estimators
    # D, RD, lD, lRD
    Y_path_region_52 = folder_path + '/dwt_ts_ROI_52.txt'
    Y_path_region_53 = folder_path + '/dwt_ts_ROI_53.txt'

    weights_path_region_52 = folder_path + '/weight_ROI_52.txt'
    weights_path_region_53 = folder_path + '/weight_ROI_53.txt'

    coords_path_region_52 = folder_path + '/coord_ROI_52.txt'
    coords_path_region_53 = folder_path + '/coord_ROI_53.txt'

    data_region_52 = process_region(
                    Y_path_region_52,
                    coords_path_region_52,
                    weights_path_region_52
                    )
    data_region_53 = process_region(
                    Y_path_region_53,
                    coords_path_region_53,
                    weights_path_region_53
                    )

    # data of the 4 regions that would be given to the estimators
    # to compute correlation the two regions of interest will be each
    # time the first elements of this array
    # regions 3 and 4 will remain the same for all pairs of regions
    data = [{}, {}, data_region_52, data_region_53]

    return data, folder_path


def calculate_matrix_of_correlation_using_one_estimator(
        estimator: str,
        numbers: list[int] = [1, 2, 7, 10]) -> np.ndarray:
    """
    Calculate matrix of correlation between regions identified with the
    numbers in the list `numbers` given as a parameter

    Parameters:
    ------------
    estimator : the name of the estimator

    numbers : numbers identifying regions that would be used

    # NB : region 3 and 4 that are uncorrelated to each other
    and other regions that are used in all estimators are already
    chosen and can not be modified

    Returns:
    -----------
    matrix of correlation between these regions
    """
    number_of_regions = len(numbers)

    data, folder_path = processing()

    # a dictionnary that contains paths of Y, coords and w of these regions
    paths = get_all_files_in_folder_matching_number(folder_path, numbers)
    # array of all possible combinations of 2 regions
    combinations_of_2_regions = list(
                            combinations([i for i in range(len(numbers))], 2)
                            )

    # a dictionnary that associate an estimator to the matrix
    # of correlation between regions
    # using that estimator
    estimator_matrix_correlation = np.zeros(
                                    (number_of_regions, number_of_regions)
                                    )

    # loop to calculate correlations between regions
    for index_region_1, index_region_2 in combinations_of_2_regions:
        # first region
        data_index_region_1 = process_region(
            Y_path=paths['Y_paths'][index_region_1],
            coords_path=paths['coords_paths'][index_region_1],
            weights_path=paths['weights_paths'][index_region_1])
        # second region
        data_index_region_2 = process_region(
            Y_path=paths['Y_paths'][index_region_2],
            coords_path=paths['coords_paths'][index_region_2],
            weights_path=paths['weights_paths'][index_region_2])
        # putting data of first and second regions in the first and
        # second index
        # of the list "data" that will contain data of the 4 regions
        data[0], data[1] = data_index_region_1, data_index_region_2
        correlation = estCorr(
            data=data, meth=estimator
            )["est"]
        estimator_matrix_correlation[
            index_region_1, index_region_2] = correlation
        estimator_matrix_correlation[
            index_region_2, index_region_1] = correlation
    return estimator_matrix_correlation
