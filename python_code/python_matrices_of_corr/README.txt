Files organization in the 'python_matrices_of_corr' directory:

"one_estimator_matrix_of_correlation.py": This file is used to
compute correlations between regions defined in the folder "one_rat_data"

"processing_1.py": This file contains the functions used to process
data from the folder one_rat_data
