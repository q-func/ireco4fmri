import numpy as np
import networkx as nx 
from auxiliary_functions import global_efficiency
from auxiliary_functions import local_efficiency
from auxiliary_functions import construct_mst_matrix
from auxiliary_functions import construct_adj_mat
from auxiliary_functions import construct_test_sign

def compute_graphs(cor_mat, cost_percentage):
    """Compute graph metrics obtained from correlation matrices

    Parameters:
    ----------
    cor_mat : ndarray
        matrix of correlation

    cost_percentage : float
        percentage of the total number of edges in the graph to be constructed (between 0 and 1)

    Returns:
    ----------
    metrics : dict
        a dictionnary of the metrics of the graph constructed containing cost edges
    """
    n_regions = cor_mat.shape[0]
    cost = int( cost_percentage * (n_regions*(n_regions-1) / 2 ) )
    abs_cor_mat = np.abs(cor_mat)
    np.fill_diagonal(abs_cor_mat, 0)

    dissimilarity_mat = 2 * (1 - abs_cor_mat)
    distances = np.sqrt(dissimilarity_mat)

    mst_matrix = construct_mst_matrix(distances)
    
    MST = mst_matrix > 0

    test_sign = construct_test_sign(MST, cor_mat, n_regions, cost)

    # Construct the adjacency matrix
    adj_mat, res_MST_edges = construct_adj_mat(MST=MST, n_regions=n_regions, test_sign=test_sign)
    print( np.sum(adj_mat) / 2)

    # Calculate global efficiency
    tmp = global_efficiency(adj_mat, weight_mat=np.ones((n_regions, n_regions))) 
    res_Eglob = tmp["nodal_eff"]
    res_mean_Eglob = tmp["eff"]
    res_degree = np.sum(adj_mat, axis=1)

    # Calculate local efficiency
    tmp1 = local_efficiency(adj_mat, weight_mat= np.ones((n_regions, n_regions))) 
    res_Eloc = tmp1["loc_eff"]
    res_mean_Eloc = tmp1["eff"]

    tmp_graph = nx.from_numpy_array(np.multiply(adj_mat, abs_cor_mat))
    # Calculate the betweenness
    betweenness_centrality = np.array(list(nx.betweenness_centrality(tmp_graph, weight='weight', normalized=False).values()))

    # Calculate transitivity
    dict_res_transitivity =  nx.clustering(tmp_graph) # transitivity weighted
    for node, coefficient in dict_res_transitivity.items():
        if not (tmp_graph.degree(node) > 1):
            dict_res_transitivity[node] = np.nan

    res_transitivity = [dict_res_transitivity[i] for  i in range(len(dict_res_transitivity))]
    res_mean_transitivity = nx.transitivity(tmp_graph) # transitivity global

    diag_abs_cor_mat = abs_cor_mat
    np.fill_diagonal(diag_abs_cor_mat, np.nan)

    # Calculate the weighted degree of each node
    weighted_degree = np.nanmean(diag_abs_cor_mat, axis=1)

    # Calculate the maximum of each node
    res_max = np.nanmax(diag_abs_cor_mat, axis = 1)

    # Calculate the strength of each node
    node_strength = dict(nx.degree(tmp_graph, weight='weight'))

    # Calculate the degree of each node
    node_degree = dict(tmp_graph.degree())
    
    # Calculate the normalized strength
    res_strength = np.array([node_strength[node] / node_degree[node] for node in tmp_graph.nodes])

    tmp_graph = nx.from_numpy_array(adj_mat)
    # Calculate the betweenness
    betweenness_unw = np.array(list(nx.betweenness_centrality(tmp_graph, normalized=False).values()))

    # construct of a dictionnary of these metrics
    metrics = {}
    metrics["adj_mat"] = adj_mat
    metrics["res_mean_transitivity"] = res_mean_transitivity
    metrics["res_Eglob"]=res_Eglob
    metrics["res_MST_edges"]=res_MST_edges
    metrics["res_degree"]=res_degree
    metrics["res_Eloc"]=res_Eloc
    metrics["betweenness_centrality"]=betweenness_centrality
    metrics["betweenness_unw"]=betweenness_unw
    metrics["weighted_degree"]=weighted_degree
    metrics["res_strength" ]= res_strength
    metrics["res_mean_Eglob"]=res_mean_Eglob
    metrics["res_mean_Eloc"] = res_mean_Eloc
    metrics["res_transitivity"] = res_transitivity
    metrics["res_mean_transitivity"] = res_mean_transitivity
    metrics["res_max"] = res_max

    return metrics
