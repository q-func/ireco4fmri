{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f81a5265",
   "metadata": {},
   "source": [
    "# <center>Map to use the different estimators</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "728d0a0b",
   "metadata": {},
   "source": [
    "This notebook aims to provide an overview of the various estimators explored in the research paper, demonstrating how our code can be utilized to implement them effectively."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "92691666",
   "metadata": {},
   "outputs": [],
   "source": [
    "# necessary librairies\n",
    "import os\n",
    "import sys\n",
    "import numpy as np\n",
    "\n",
    "# Get the absolute path to the needed files and append it to sys.path\n",
    "sys.path.append(os.path.abspath(os.path.join(os.pardir, \"simulations\")))\n",
    "sys.path.append(os.path.abspath(os.path.join(os.pardir, \"src\")))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "546a9337",
   "metadata": {},
   "source": [
    "## 1. Simulated data\n",
    "In this notebook, our focus is on evaluating the performance of estimators using a simulated dataset. To generate this dataset, we will carefully select specific parameters that will be utilized as input for the `build_sim_data` function.\n",
    "\n",
    "Using the `build_sim_data` function, we will simulate two distinct regions: one comprising 20 voxels and the other consisting of 40 voxels. To capture a moderate level of correlation, we will set the Inter-Region Correlation parameter to 0.6. Additionally, to analyze estimators based on differences, namely methods D, lD, and lRD, we will introduce two disconnected regions that do not overlap with the initial regions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "3375a882",
   "metadata": {},
   "outputs": [],
   "source": [
    "# importing the build_sim_data function\n",
    "from simulations import build_sim_data \n",
    "\n",
    "# setting the seed to get reproducible results\n",
    "np.random.seed(42) \n",
    "\n",
    "# Simulating Two Regions\n",
    "\n",
    "# Parameters\n",
    "n = 1000  # number of samples\n",
    "p1 = 20  # number of variables in region 1\n",
    "p2 = 40  # number of variables in region 2\n",
    "p3 = 40  # number of variables in region 3\n",
    "p4 = 40  # number of variables in region 4\n",
    "\n",
    "rinter = 0.6  # inter-region correlation\n",
    "\n",
    "D = 100  # parameter to control the homogeneity of the region\n",
    "rmin = 0.6  # intra-correlation parameter\n",
    "\n",
    "vareps = 0  # variance of the noise\n",
    "vare = 0  # variance of the global noise\n",
    "\n",
    "# Simulate data\n",
    "simu = build_sim_data(T=n, rmin=rmin, D=D, vareps=vareps, vare=vare, vecn=[p1, p2, p3, p4], rinter=rinter)\n",
    "data = simu[\"data\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "985bccfb",
   "metadata": {},
   "source": [
    "## 2.  Inter-correlation estimators\n",
    "Now, we move on to evaluating our estimators using the dataset we generated earlier. The dataset, stored in the `data` variable, will be utilized for this evaluation."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "367b5abb",
   "metadata": {},
   "source": [
    "### 2.1 Correlation of averages: Method CA"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e39b241b",
   "metadata": {},
   "source": [
    "This estimator involves averaging (or occasionally convolving with a Gaussian kernel) the signal within each region of interest. The aggregated correlation estimator corresponds to the standard estimator:\n",
    "<center>$\\hat{r}_{jj}^{CA}= \\frac{\\hat{\\text{Cov}}(\\bar{Y}_{Rj}, \\bar{Y}_{Rj'})}{\\hat{\\sigma}(\\bar{Y}_{Rj})\\hat{\\sigma}(\\bar{Y}_{Rj'})}$</center>\n",
    "\n",
    "In this formula, $\\bar{Y}_{Rj}$ represents the average signal in region $Rj$ and $\\bar{Y}_{Rj}$ denotes the standard deviation of the average signal in region $Rj$.\n",
    "\n",
    "The numerator of the fraction calculates the covariance between the average signals of regions \n",
    "$Rj$ and $Rj'$, while the denominator accounts for the standard deviations of the average signals in each region."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "62fd345f",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.6651286833349597"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Import the CA class from the CA module\n",
    "from CA import CA\n",
    "\n",
    "# Extract the wavelet data associated with the two regions of interest\n",
    "Y0 = data[0][\"Y\"]\n",
    "Y1 = data[1][\"Y\"]\n",
    "\n",
    "# Create an instance of the CA class and call its 'estimator_correlation' method\n",
    "result = CA.estimator_correlation(Y0, Y1)\n",
    "\n",
    "result[\"est\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4c3ce7f4",
   "metadata": {},
   "source": [
    "### 2.2 Average of correlations: Method AC"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2b032209",
   "metadata": {},
   "source": [
    "This estimator calculates the average of correlations across spatial locations, rather than evaluating the correlation of spatial averages. It is defined as follows:\n",
    "\n",
    "<center>$\n",
    "\\hat{r}_{jj'}^{AC} = \\frac{{\\sum_{{i \\in R_j, i' \\in R_j'}} \\hat{\\text{Cor}}(Y_{Ri}, Y_{Ri'})}}{{N_j \\cdot N_{j'}}}\n",
    "$</center>\n",
    "\n",
    "In this formula, $\\hat{r}_{jj'}^{AC}$ represents the estimated intercorrelation between regions $R_j$ and $R_{j'}$. The numerator of the fraction sums up the estimated covariance between the signals $Y_{Ri}$ and $Y_{Ri'}$ across all voxel pairs $(i, i')$ where $i$ belongs to region $R_j$ and $i'$ belongs to region $R_{j'}$. The denominator accounts for the number of voxels in each region, denoted as $N_j$ and $N_{j'}$ respectively."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "fb84f162",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.5998654170184068"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Import the AC class from the AC module\n",
    "from AC import AC\n",
    "\n",
    "# Extract the wavelet data associated with the two regions of interest\n",
    "Y0 = data[0][\"Y\"]\n",
    "Y1 = data[1][\"Y\"]\n",
    "\n",
    "# Create an instance of the AC class and call its 'estimator_correlation' method\n",
    "result = AC.estimator_correlation(Y0, Y1)\n",
    "\n",
    "result[\"est\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a0e5a814",
   "metadata": {},
   "source": [
    "### 2.3 Replicates for correlations: Method R"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c08c3b69",
   "metadata": {},
   "source": [
    "This estimator is  based on the concept of replicates within the same region, and denoted by $\\hat{r}^R$. It is defined by:\n",
    "<center>$ \\hat{r}_{jj'}^R = \\frac{1}{B} \\sum_{b=1}^B \\frac{\\frac{1}{4} \\sum_{\\sigma, \\beta = 1}^2 \\hat{\\text{Cor}}\n",
    "(Y_{i_{\\alpha}^{(b)}}, Y_{{i'}_{\\alpha}^{(b)}})}{\\sqrt{|\\hat{\\text{Cor}}\n",
    "(Y_{i_{1}^{(b)}}, Y_{i_{2}^{(b)}}) \\hat{\\text{Cor}}\n",
    "(Y_{{i'}_{1}^{(b)}}, Y_{{i'}_{2}^{(b)}})|}}$</center>\n",
    "\n",
    "where  for $b = 1,...,B$, $i_{1}^{(b)}, i_{2}^{(b)} \\in R_j$ are such that $|i_{1}^{(b)}- i_{2}^{(b)}| = \\delta \\geq p$. Inthe same way, ${i'}_{1}^{(b)}, {i'}_{2}^{(b)} \\in R_j'$ are such that $|{i'}_{1}^{(b)}- {i'}_{2}^{(b)}| = \\delta \\geq p$ (meaning that the noise is p-dependent)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "13d2d0b3",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.6056779183433808"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Import the R class from the R module\n",
    "from R import R\n",
    "\n",
    "# Import the cdist function from the scipy.spatial.distance module\n",
    "from scipy.spatial.distance import cdist\n",
    "\n",
    "# Extract the wavelet data associated with the two regions of interest\n",
    "Y0 = data[0][\"Y\"]\n",
    "Y1 = data[1][\"Y\"]\n",
    "\n",
    "# Extract the coordinates data associated with the two regions of interest\n",
    "coords0 = data[0][\"coords\"]\n",
    "coords1 = data[1][\"coords\"]\n",
    "\n",
    "\n",
    "# Calculate the pairwise Chebyshev distance between the coordinates \n",
    "# in the two regions of interest\n",
    "d0 = cdist(coords0.T, coords0.T, metric=\"chebyshev\")\n",
    "d1 = cdist(coords1.T, coords1.T, metric=\"chebyshev\")\n",
    "\n",
    "# Set the others parameters necessary to this method\n",
    "B = 500\n",
    "dRepl = 1\n",
    "\n",
    "# Create an instance of the R class and call its 'estimator_correlation' method\n",
    "result = R.estimator_correlation(Y0=Y0, Y1=Y1, d0=d0, d1=d1, B=B, dRepl=dRepl)\n",
    "\n",
    "result[\"est\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d9d1f949",
   "metadata": {},
   "source": [
    "### 2.4 Use of a priori uncorrelated regions: Method D based on differences"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "094e9279",
   "metadata": {},
   "source": [
    "This estimator assumes the presence of at least two regions, denoted as $R_k$ and $R_k′$, among the recorded signal regions. These two regions are considered to be uncorrelated with each other and with all other regions. For the sake of simplicity, we will use the indices $k$ and $k′$ to refer to these two specific regions, while indices $j$ and $j′$ will be used to denote the inter-correlation between regions $Rj$ and $Rj'$ (thus implying that $rjk = rjk′ = rj′k = rj′k′ = 0$). And we propose the following strategy:\n",
    "\n",
    "\n",
    "for $b = 1, . . . , B$ let $i^{b}, {i'}^{b}, k^{b}$ and ${k'}^{b}$ be voxels of $Rj, Rj′, R_k$ and $R_k′$:\n",
    "\n",
    "  \n",
    "<center>        \n",
    "$\\hat{r}_{jj'}^D = \\frac{1}{B} \\sum_{b=1}^B \\tilde{Cor}(Y_{i^{(b)}}, Y_{{i'}^{(b)}} ;Y_{k^{(b)}} , Y_{{k'}^{(b)}})$\n",
    "</center>\n",
    "\n",
    "Where for four vectors $Y_1, Y_2, Y_3$ and $Y_4$ with same length: \n",
    "$ \\quad \\tilde{Cor}(Y_1, Y_2; Y_3, Y_4) = \\frac{\\hat{\\text{Cor}}(Y_1 - Y_3, Y_2 - Y_4)}{\\hat{s}(Y_1, Y_3, Y_4) \\hat{s}(Y_2, Y_3, Y_4)}$\n",
    "\n",
    "and where for three vectors $U, V$ and $W$ with same length:\n",
    "$\\quad \\hat{s}^2 = (\\hat{\\sigma}^2(U-V) + \\hat{\\sigma}^2(U-W) + \\hat{\\sigma}^2(V-W)) \\; / \\; 2.$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "588aa2bf",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.6241682920052465"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Import the D class from the D module\n",
    "from D import D\n",
    "\n",
    "# Extract the wavelet data associated with the two regions of interest\n",
    "Y0 = data[0][\"Y\"]\n",
    "Y1 = data[1][\"Y\"]\n",
    "\n",
    "# Extract the wavelet data associated with the two other regions needed for calculation\n",
    "Y2 = data[2][\"Y\"]\n",
    "Y3 = data[3][\"Y\"]\n",
    "\n",
    "# Set the others parameters necessary to this method\n",
    "B = 500\n",
    "\n",
    "# Create an instance of the D class and call its 'estimator_correlation' method\n",
    "result = D.estimator_correlation(Y0, Y1, Y2, Y3, B)\n",
    "\n",
    "result[\"est\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "90724ea8",
   "metadata": {},
   "source": [
    "### 2.5 Replicates and use of a priori disconnected regions: Method RD"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5eef46e9",
   "metadata": {},
   "source": [
    "This estimator combine replicates and the idea based on differences:\n",
    "    <center>$ \\hat{r}_{jj'}^{RD} = \\frac{1}{B} \\sum_{b=1}^B \\frac{\\frac{1}{4} \\sum_{\\sigma, \\beta = 1}^2 \\tilde{Cor}\n",
    "(Y_{i_{\\alpha}^{(b)}}, Y_{{i'}_{\\alpha}^{(b)}}; Y_{k^{(b)}} , Y_{{k'}^{(b)}})}{\\sqrt{|\\tilde{Cor}\n",
    "(Y_{i_{1}^{(b)}}, Y_{i_{2}^{(b)}}; Y_{k^{(b)}} , Y_{{k'}^{(b)}}) \\; \\tilde{Cor}\n",
    "(Y_{{i'}_{1}^{(b)}}, Y_{{i'}_{2}^{(b)}}; Y_{k^{(b)}} , Y_{{k'}^{(b)}})|}}$</center>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "dcb7636d",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.6303535308412801"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Import the RD class from the RD module\n",
    "from RD import RD\n",
    "\n",
    "# Extract the wavelet data associated with the two regions of interest\n",
    "Y0 = data[0][\"Y\"]\n",
    "Y1 = data[1][\"Y\"]\n",
    "\n",
    "# Extract the wavelet data associated with the two other regions needed for calculation\n",
    "Y2 = data[2][\"Y\"]\n",
    "Y3 = data[3][\"Y\"]\n",
    "\n",
    "# Extract the coordinates data associated with the two regions of interest\n",
    "coords0 = data[0][\"coords\"]\n",
    "coords1 = data[1][\"coords\"]\n",
    "\n",
    "\n",
    "# Calculate the pairwise Chebyshev distance between the coordinates \n",
    "# in the two regions of interest\n",
    "d0 = cdist(coords0.T, coords0.T, metric=\"chebyshev\")\n",
    "d1 = cdist(coords1.T, coords1.T, metric=\"chebyshev\")\n",
    "\n",
    "# Set the others parameters necessary to this method\n",
    "B = 500\n",
    "dRepl = 1\n",
    "\n",
    "# Create an instance of the RD class and call its 'estimator_correlation' method\n",
    "result = RD.estimator_correlation(Y0, Y1, Y2, Y3, d0, d1, B, dRepl)\n",
    "\n",
    "result[\"est\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6261d120",
   "metadata": {},
   "source": [
    "### 2.6 Replicates and use of a priori disconnected regions: Method lCA"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e3f9bd0e",
   "metadata": {},
   "source": [
    "This estimator is motivated by the first two estimators and using an empirical average of local spatial averages.\n",
    "\n",
    "\n",
    "For $b = 1, . . . , B$, let $\\nu_j^{(b)}$ (resp. $\\nu_{j'}^{(b)}$ be a $\\nu$-neighborhood of $Rj$ (resp. $Rj′$).\n",
    "\n",
    "<center>$\\hat{r}_{jj}^{lCA}= \\frac{1}{B} \\sum_{b=1}^B \\hat{\\text{Cor}}(\\bar{Y}_{\\nu_j^{(b)}}, \\bar{Y}_{\\nu_{j'}^{(b)}})$</center>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "19f157a9",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.6049453316553085"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Import the lCA class from the lCA module\n",
    "from lCA import lCA\n",
    "\n",
    "# Extract the wavelet data associated withthe two regions of interest\n",
    "Y0 = data[0][\"Y\"]\n",
    "Y1 = data[1][\"Y\"]\n",
    "\n",
    "# Extract the coordinates data associated with the two regions of interest\n",
    "coords0 = data[0][\"coords\"]\n",
    "coords1 = data[1][\"coords\"]\n",
    "\n",
    "\n",
    "# Calculate the pairwise Chebyshev distance between the coordinates \n",
    "# in the two regions of interest\n",
    "d0 = cdist(coords0.T, coords0.T, metric=\"chebyshev\")\n",
    "d1 = cdist(coords1.T, coords1.T, metric=\"chebyshev\")\n",
    "\n",
    "# Set the others parameters necessary to this method\n",
    "B = 500\n",
    "dNeigh = 1\n",
    "\n",
    "# Create an instance of the lCA class and call its 'estimator_correlation' method\n",
    "result = lCA.estimator_correlation(Y0, Y1, d0, d1, B, dNeigh)\n",
    "\n",
    "result[\"est\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "db96e443",
   "metadata": {},
   "source": [
    "### 2.7 Local average of replicates: Method lR"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "06699f94",
   "metadata": {},
   "source": [
    "This estimator consists in replacing single indices by neighborhoods in the third estimator presented in the section 2.3.\n",
    "\n",
    "For $b = 1,...,B$, let $\\nu_{j_1}^{(b)}$ and $\\nu_{j_2}^{(b)}$ (resp $\\nu_{{j'}_1}^{(b)}$ and $\\nu_{{j'}_2}^{(b)}$) be two $\\nu$-neighborhoods in $Rj$ (resp. $Rj'$) such that \n",
    "\n",
    "\n",
    "for any $ i_1^{(b)} \\in \\nu_{j_1}^{(b)}, i_2^{(b)} \\in \\nu_{j_2}^{(b)}, |i_1^{(b)} - i_2^{(b)}| = \\delta \\geq p$ \n",
    "\n",
    "(resp $|{i'}_1^{(b)} - {i'}_2^{(b)}| = \\delta \\geq p$ \\; for any ${i'}_1^{(b)} \\in \\nu_{{j'}_1}^{(b)}, {i'}_2^{(b)} \\in \\nu_{{j'}_2}^{(b)}$).\n",
    "\n",
    "\n",
    "The local average of replicates based estimator is defined by:\n",
    "<center>$ \\hat{r}_{jj'}^{lR} = \\frac{1}{B} \\sum_{b=1}^B \\frac{\\frac{1}{4} \\sum_{\\sigma, \\beta = 1}^2 \\hat{\\text{Cor}}\n",
    "(\\bar{Y}_{\\nu_{j_\\alpha}^{(b)}}, \\bar{Y}_{\\nu_{{j'}_\\beta}^{(b)}})}{\\sqrt{|\\hat{\\text{Cor}}\n",
    "(\\bar{Y}_{\\nu_{j_1}^{(b)}}, \\bar{Y}_{\\nu_{j_2}^{(b)}}) \\hat{\\text{Cor}}\n",
    "(\\bar{Y}_{\\nu_{{j'}_1}^{(b)}}, \\bar{Y}_{\\nu_{{j'}_2}^{(b)}})|}}$</center>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "ba931c32",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.6186464495441588"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Import the lR class from the lR module\n",
    "from lR import lR\n",
    "\n",
    "# Extract the wavelet data associated with the two regions of interest\n",
    "Y0 = data[0][\"Y\"]\n",
    "Y1 = data[1][\"Y\"]\n",
    "\n",
    "# Extract the coordinates data associated with the two regions of interest\n",
    "coords0 = data[0][\"coords\"]\n",
    "coords1 = data[1][\"coords\"]\n",
    "\n",
    "\n",
    "# Calculate the pairwise Chebyshev distance between the coordinates \n",
    "# in the two regions of interest\n",
    "d0 = cdist(coords0.T, coords0.T, metric=\"chebyshev\")\n",
    "d1 = cdist(coords1.T, coords1.T, metric=\"chebyshev\")\n",
    "\n",
    "# Set the others parameters necessary to this method\n",
    "B = 500\n",
    "dRepl = 1\n",
    "dNeigh = 1\n",
    "\n",
    "\n",
    "# Create an instance of the RD class and call its 'estimator_correlation' method\n",
    "result = lR.estimator_correlation(Y0, Y1, d0, d1, B, dRepl, dNeigh)\n",
    "\n",
    "result[\"est\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3704e391",
   "metadata": {},
   "source": [
    "### 2.8 Local averages and use of disconnected regions: Method lD"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eeb7daa2",
   "metadata": {},
   "source": [
    "We use in particular notation introduced in Sections 2.7 and 2.4 to propose the estimator $\\hat{r}_{jj'}^{lD}$ given by:\n",
    "\n",
    "<center>        \n",
    "$\\hat{r}_{jj'}^D = \\frac{1}{B} \\sum_{b=1}^B \\tilde{Cor}(\\bar{Y}_{\\nu_{j}^{(b)}}, \\bar{Y}_{\\nu_{j'}^{(b)}} ;\\bar{Y}_{\\nu_{k}^{(b)}} , \\bar{Y}_{\\nu_{k'}^{(b)}})$\n",
    "</center>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "893aa5fa",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.6294782249745444"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Import the lD class from the lD module\n",
    "from lD import lD\n",
    "\n",
    "# Extract the wavelet data associated with the two regions of interest\n",
    "Y0 = data[0][\"Y\"]\n",
    "Y1 = data[1][\"Y\"]\n",
    "\n",
    "# Extract the wavelet data associated with the two other regions needed for calculation\n",
    "Y2 = data[2][\"Y\"]\n",
    "Y3 = data[3][\"Y\"]\n",
    "\n",
    "# Extract the coordinates data associated with thw four regions\n",
    "coords0 = data[0][\"coords\"]\n",
    "coords1 = data[1][\"coords\"]\n",
    "coords2 = data[2][\"coords\"]\n",
    "coords3 = data[3][\"coords\"]\n",
    "\n",
    "# Calculate the pairwise Chebyshev distance between the coordinates \n",
    "# in the four regions\n",
    "d0 = cdist(coords0.T, coords0.T, metric=\"chebyshev\")\n",
    "d1 = cdist(coords1.T, coords1.T, metric=\"chebyshev\")\n",
    "d2 = cdist(coords2.T, coords2.T, metric=\"chebyshev\")\n",
    "d3 = cdist(coords3.T, coords3.T, metric=\"chebyshev\")\n",
    "\n",
    "# Set the others parameters necessary to this method\n",
    "B = 500\n",
    "dNeigh = 1\n",
    "\n",
    "\n",
    "# Create an instance of the lD class and call its 'estimator_correlation' method\n",
    "result = lD.estimator_correlation(Y0, Y1, Y2, Y3, d0, d1, d2, d3, B, dNeigh)\n",
    "\n",
    "result[\"est\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11bc228e",
   "metadata": {},
   "source": [
    "### 2.9 Replicates, local averages and use of a priori disconnected regions: Method lRD"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "96e5ab47",
   "metadata": {},
   "source": [
    "This estimator is a local version of $\\hat{r}_{jj'}^{RD}$ and is defined by:\n",
    "\n",
    "<center>$ \\hat{r}_{jj'}^{RD} = \\frac{1}{B} \\sum_{b=1}^B \\frac{\\frac{1}{4} \\sum_{\\sigma, \\beta = 1}^2 \\tilde{Cor}\n",
    "(\\bar{Y}_{\\nu_{j_\\alpha}^{(b)}}, \\bar{Y}_{\\nu_{{j'}_\\beta}^{(b)}}; \\bar{Y}_{\\nu_{k}^{(b)}} , \\bar{Y}_{\\nu_{k'}^{(b)}})}\n",
    "{\\sqrt{|\\tilde{Cor}\n",
    "\\bar{Y}_{\\nu_{j_1}^{(b)}}, \\bar{Y}_{\\nu_{j_2}^{(b)}}; \\bar{Y}_{\\nu_{k}^{(b)}} , \\bar{Y}_{\\nu_{k'}^{(b)}}) \\; \\tilde{Cor}\n",
    "(\\bar{Y}_{\\nu_{{j'}_1}^{(b)}}, \\bar{Y}_{\\nu_{{j'}_2}^{(b)}}; \\bar{Y}_{\\nu_{k}^{(b)}} , \\bar{Y}_{\\nu_{k'}^{(b)}})|}}$</center>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "ea87c52b",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.6413159664711491"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Import the lRD class from the lRD module\n",
    "from lRD import lRD\n",
    "\n",
    "# Extract the wavelet data associated with the two regions of interest\n",
    "Y0 = data[0][\"Y\"]\n",
    "Y1 = data[1][\"Y\"]\n",
    "\n",
    "# Extract the wavelet data associated with the two other regions needed for calculation\n",
    "Y2 = data[2][\"Y\"]\n",
    "Y3 = data[3][\"Y\"]\n",
    "\n",
    "# Extract the coordinates data associated with thw four regions\n",
    "coords0 = data[0][\"coords\"]\n",
    "coords1 = data[1][\"coords\"]\n",
    "coords2 = data[2][\"coords\"]\n",
    "coords3 = data[3][\"coords\"]\n",
    "\n",
    "# Calculate the pairwise Chebyshev distance between the coordinates \n",
    "# in the four regions\n",
    "d0 = cdist(coords0.T, coords0.T, metric=\"chebyshev\")\n",
    "d1 = cdist(coords1.T, coords1.T, metric=\"chebyshev\")\n",
    "d2 = cdist(coords2.T, coords2.T, metric=\"chebyshev\")\n",
    "d3 = cdist(coords3.T, coords3.T, metric=\"chebyshev\")\n",
    "\n",
    "# Set the others parameters necessary to this method\n",
    "B = 500\n",
    "dRepl = 1\n",
    "dNeigh = 1\n",
    "\n",
    "\n",
    "# Create an instance of the lRD class and call its 'estimator_correlation' method\n",
    "result = lRD.estimator_correlation(Y0, Y1, Y2, Y3, d0, d1, d2, d3, B, dNeigh, dRepl)\n",
    "\n",
    "result[\"est\"]"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.17"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
