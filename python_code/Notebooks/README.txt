Files organization in the 'Notebooks' directory:

'Estimators.ipynb': This Jupyter notebook provides an explanation of how to use our various estimators.
                    These estimators are applied to two simulated regions, and the notebook guides 
                    users on their usage.

'Rat_Map.ipynb': In this Jupyter notebook, users can learn how to utilize our different estimators 
                 on a rat brain data. Additionally, it demonstrates how to visualize the results 
                 and the associated metrics.

'auxiliary_functions.py': This Python file contains several auxiliary functions that are used in 
                          the function_compute_graphs.py file. These functions support the main 
                          computations for the brain's graph analysis.

'function_compute_graphs.py': The main functionality of this Python file is to calculate the 
                              metrics of the brain's graph. 

'processing-1.py': This Python file contains various functions that are involved in processing 
                   and ordering data.
