import numpy as np
import networkx as nx

def global_efficiency(adj_mat, weight_mat):
        """calculates the global efficiency of a graph.

        The adjacency matrix is transformed to a graph before calculating the efficiency

        Parameters:
        ----------
        adj_mat : ndarray 
            adjacency matrix of a graph 
        
        weight_mat : ndarray
            matrix containing the weights of the edges


        Returns:
        ----------
        effiency : dict
            A dictionnary containing the global efficiency(mean) and the nodal efficiency from the graph.
        """
        # Create an empty graph
        graph = nx.Graph()

        # Add edges based on the adjacency matrix
        num_nodes = adj_mat.shape[0]
        for i in range(num_nodes):
            for j in range(i + 1, num_nodes):
                if adj_mat[i, j] == 1:
                    #graph.add_edge(i, j)
                    # If the graph is weighted, use the following line
                    graph.add_edge(i, j, weight=weight_mat[i][j])

        # Calculate nodal efficiency
        nodal_efficiency = []
        for node in sorted(graph.nodes()):
            shortest_paths = nx.shortest_path_length(graph, node)
            inv_distances = [1 / length for length in shortest_paths.values() if length != 0]
            if len(inv_distances) == 0:
                nodal_efficiency.append(0)
            else:
                nodal_efficiency.append(sum(inv_distances) / (num_nodes - 1))

        # Calculate global efficiency
        global_efficiency = np.mean(nodal_efficiency) #nx.global_efficiency(graph)
        efficiency = {"eff": global_efficiency, "nodal_eff": nodal_efficiency}
        return efficiency



def local_efficiency(adj_mat, weight_mat=None):
    """Calculates the efficiency and the local efficiency of a graph.

    Parameters:
    ----------
    adj_mat : ndarray
        adjacency matrix of a graph 
    
    weight_mat : ndarray 
        matrix containing the weights of the edges

    Returns:
    ----------
    efficiency : dict
        A dictionnary containing the efficiency(the mean) and the local efficiency 
    """
    # Create a graph from the adjacency matrix
    G = nx.from_numpy_array(adj_mat)
    
    # Compute the shortest path lengths between all pairs of nodes
    if weight_mat is None:
        shortest_paths = dict(nx.all_pairs_shortest_path_length(G))
    else:
        # If the graph is weighted, use Dijkstra's algorithm for efficiency
        shortest_paths = dict(nx.all_pairs_dijkstra_path_length(G, weight='weight'))
    
    # Compute local efficiency for each node
    local_efficiency_values = []
    for node in G.nodes():
        neighbors = list(G.neighbors(node))
        ngi = len(neighbors)
        
        local_efficiency = 0.0
        subgraph = G.subgraph(neighbors)

        if weight_mat is None:
            sub_shortest_paths = dict(nx.all_pairs_shortest_path_length(subgraph))
        else:
            sub_shortest_paths = dict(nx.all_pairs_dijkstra_path_length(subgraph, weight='weight'))
        for j in range(ngi - 1):
            for k in range(j + 1, ngi):
                if neighbors[j] in sub_shortest_paths and neighbors[k] in sub_shortest_paths[neighbors[j]]:
                    shortest_path_length = sub_shortest_paths[neighbors[j]][neighbors[k]]
                    local_efficiency += 2.0 / shortest_path_length
        
        if ngi > 1:
            local_efficiency /= (ngi * (ngi - 1))
        
        local_efficiency_values.append(local_efficiency)
    
    # Compute the mean of local efficiency for the whole graph
    mean_local_efficiency = np.mean(local_efficiency_values)
    
    efficiency = {"eff": mean_local_efficiency, "loc_eff": local_efficiency_values}
    return efficiency


def construct_mst_matrix(matrix):
    """Construct a minimum spanning tree
    
    The matrix is transformed to a graph in order to compute a minimum spanning tree 

    Parameters:
    ----------
    matrix : ndarray
        The matrix that would be transformed to a graph

    Returns:
    ----------
    mst_matrix : ndarrau
        A minimum spanning tree of the graph
    """

    # Create a graph
    G = nx.Graph()

    # Add edges with weights from the matrix
    for k in range(len(matrix)):
        for l in range(k + 1, len(matrix[k])):
            if matrix[k][l] != 0:
                G.add_edge(k, l, weight=matrix[k][l])

    # Calculate the minimum spanning tree
    mst_tree = nx.minimum_spanning_tree(G)

    # Create an empty matrix for the minimum spanning tree
    mst_matrix = np.zeros((len(matrix), len(matrix)))

    # Update the matrix with the edges from the MST
    for u, v, data in mst_tree.edges(data=True):
        weight = data['weight']
        mst_matrix[u][v] = weight
        mst_matrix[v][u] = weight
    
    return mst_matrix

def construct_adj_mat(MST, n_regions,test_sign):
    """Construct the adjacency matrix
    
    Construct the adjacency matrix from the minimum spanning tree 

    Parameters:
    ----------
    MST : ndarray
        minimum spanning tree 
    
    n_regions : int
        number of regions 
    - test_sign : list
        a list of booleans 
    Returns:
    ----------
    The adjacency matrix 
    """
    # Construct of the adjacency matrix
    adj_mat = np.zeros((n_regions, n_regions))
    l = 0
    res_MST_edges = 0
    for k in range(2,n_regions+1):
        for q in range(1, k):
            if test_sign[l] == True:
                adj_mat[k-1, q-1] = 1
            else:
                if MST[k-1, q-1] == True:
                    adj_mat[k-1, q-1] = 1
                    res_MST_edges = res_MST_edges + 1
            l += 1

    adj_mat = adj_mat + adj_mat.T
    return adj_mat, res_MST_edges

def initialize(name_dirs_length, number_of_estimators, number_of_sessions, n_regions):
    """Initialize matrices of graph metrics variables with zeros

    Parameters:
    ----------
    name_dirs_length : int
        number of directories

    number_of_estimators : int
        number of estimators

    number_of_sessions : int
        number of sessions

    n_regions : int
        number of regions

    Returns:
    ----------
    List of graph metrics that have been initialized with zero values.
    """

    res_Eglob = np.zeros((name_dirs_length, number_of_estimators, number_of_sessions, n_regions))
    res_MST_edges = np.zeros((name_dirs_length, number_of_estimators, number_of_sessions))
    res_degree = np.zeros((name_dirs_length, number_of_estimators, number_of_sessions, n_regions))
    res_Eloc = np.zeros((name_dirs_length, number_of_estimators, number_of_sessions, n_regions))
    betweenness_centrality = np.zeros((name_dirs_length, number_of_estimators, number_of_sessions, n_regions))
    betweenness_unw = np.zeros((name_dirs_length, number_of_estimators, number_of_sessions, n_regions))
    weighted_degree = np.zeros((name_dirs_length, number_of_estimators, number_of_sessions, n_regions))
    res_strength = np.zeros((name_dirs_length, number_of_estimators, number_of_sessions, n_regions))
    res_mean_Eglob = np.zeros((name_dirs_length, number_of_estimators, number_of_sessions))
    res_mean_Eloc = np.zeros((name_dirs_length, number_of_estimators, number_of_sessions))
    res_transitivity = np.zeros((name_dirs_length, number_of_estimators, number_of_sessions, n_regions))
    res_mean_transitivity = np.zeros((name_dirs_length, number_of_estimators, number_of_sessions))
    res_max = np.zeros((name_dirs_length, number_of_estimators, number_of_sessions, n_regions))

    return [res_Eglob, res_MST_edges, res_degree, res_Eloc, betweenness_centrality, betweenness_unw, 
            weighted_degree, res_strength, res_mean_Eglob, res_mean_Eloc, res_transitivity, res_mean_transitivity, res_max]

def construct_test_sign(MST, cor_mat, n_regions, cost):
    """Construct a list of booleans

    Parameters:
    ----------
    MST : ndarray
        A minimum spanning tree
    
    cor_mat : ndarray
        correlation matrix

    n_regions : ndarray
        number of regions
        
    cost : int 
        number of edges in the graph that will be constructed later

    Returns:
    ----------
    List of booleans 
    """
    
    MST_adj_mat = MST.copy()
    MST_adj_mat = np.triu(MST_adj_mat)

    mat_pvalue_cor_intermediaire =  np.triu(cor_mat, k = 1).T
    pvalue_cor = np.abs(mat_pvalue_cor_intermediaire[np.tril_indices(cor_mat.shape[0], k = -1)])

    mat_MST_no_edges_intermediaire =  np.triu(MST_adj_mat, k = 1).T
    MST_no_edges  = mat_MST_no_edges_intermediaire[np.tril_indices(MST_adj_mat.shape[0],k = -1)] == 0
    cor_wo_MST = pvalue_cor[MST_no_edges]
    pvalue_thresh = sorted(cor_wo_MST, reverse=True)[cost - (n_regions - 1) - 1]
    test_sign = (pvalue_cor >= pvalue_thresh)

    return test_sign