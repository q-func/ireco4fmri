import pandas as pd
import numpy as np
import os
import sys

# Get the absolute path to the needed files and append it to sys.path
sys.path.append(os.path.abspath(os.path.join(os.pardir, "src")))

# Import the estCorr function
from estCorr import estCorr


def get_all_files_in_folder_matching_number(
        folder_path="/Users/ayoubcherkaoui/Documents/code_stage/" +
        "RatDataPreprocessing/ratdatapreprocessing/RatDataPreprocessed/EtoL/" +
        "20160616_103405_fmri_flat/preprocessed_20160616_103405",
        numbers=[1, 2, 4]):
    """Computes (for a rat, for example) the paths to 'Y', 'coords', and 'w'
    of the regions that
        the number that identifies them is in the list 'numbers'.

    Parameters:
    ----------
    folder_path : str
        The directory path to the folder containing files that include 'Y',
        'coords', and 'w' information.

    numbers :  list of ints
        The numbers of regions of interest.


    Returns:
    ----------
    dictionnary_paths : dict
        A dictionnary containing the paths to 'Y', 'coords' and 'w' of
        the regions corresponding to 'numbers'.
    """

    def extract_number_from_filename(filename):
        """Employs regular expressions in order to retrieve the numerical
        value that represents a particular region.

        Parameters:
        ----------
        filename : str
            name of a file

        Returns:
        ----------
        dictionnary_paths : dict
            A numerical value that represents a particular region if it exists.
        """
        import re
        pattern = r'\d+'
        match = re.search(pattern, filename)
        if match:
            return int(match.group())
        else:
            raise ValueError("This file doesn't identify any region")

    # file that will contain paths to 'Y' of the regions
    Y_paths = ['']*len(numbers)

    # file that will contain paths to 'coords' of the regions
    coords_paths = ['']*len(numbers)

    # file that will contain paths to 'w' of the regions
    weights_path = ['']*len(numbers)

    # This is a dictionary that maps a numeric identifier of a region to
    # its corresponding index in the list called 'numbers'.
    dict_numbers = {numbers[i]: i for i in range(len(numbers))}

    # processing a file to know if its path should be added to either
    # the paths of 'Y', 'coords' or 'w'
    for file_name in os.listdir(folder_path):
        number = extract_number_from_filename(file_name)
        if (number in dict_numbers.keys()):
            file_path = os.path.join(folder_path, file_name)
            if os.path.isfile(file_path):
                if (file_name[:2] == "we"):
                    weights_path[dict_numbers[number]] = file_path
                elif (file_name[:2] == "co"):
                    coords_paths[dict_numbers[number]] = file_path
                elif (file_name[:6] == "dwt_ts"):
                    Y_paths[dict_numbers[number]] = file_path
                elif (file_name[:6] == "dwt_no"):
                    pass
                elif (file_name[:2] == "ts"):
                    pass
                else:
                    raise ValueError(
                        "The name of the file is not a standard name!"
                        )

    # A dictionnary that contains the paths to 'Y', 'coords' and 'w'
    dictionnary_paths = {"Y_paths": Y_paths, "weights_paths": weights_path,
                         "coords_paths": coords_paths}

    return dictionnary_paths


def read(file_path, name="Y"):
    """Extract the data in a file from the path to the file

    Parameters:
    ----------
    file_path : str
        the path to the file

    name : str
        name of the data to extract

    Returns:
    ----------
    array : ndarray
       The data in a file as a numpy array
    """

    with open(file_path, 'r') as f:
        lines = f.readlines()

    if (name == "coords"):
        # Extracting coordinates
        lines = lines[1:]  # Ignore the first line containing the region name

    # Extracting floats
    floats = [list(map(float, line.strip().split())) for line in lines]

    # Create the array with dimensions (number of floats, number of lines)
    array = np.array(floats).T
    if (name == "w"):
        array = array.reshape((array.shape[1],))

    return array


def add_to_list_Y_coords_w(liste_Y, _liste_coords, liste_w):
    """Puts in a dictionnary the data 'Y', 'coords' and 'w'
    having as keys 'Y', 'coords' and 'w'

    Parameters:
    ---------
    liste_Y : list of ndarrays
        liste of all the 'Y' data

    liste_coords : list of ndarrays
        liste of all the 'coords' data

    liste_w: list of ndarrays
        liste of all the 'w' data

    Returns:
    ---------
    data : dict
        A dictionnary associating the data Y to 'Y',
        the data coords to 'coords' and the data w to 'w'
    The data in a file as a numpy array
    """
    data = []
    for Y, coords, w in zip(liste_Y, _liste_coords, liste_w):
        data.append({'Y': Y, 'coords': coords, 'w': w})
    return data


def estimators_corr(data, number_of_simulations=1,
                    estimators=["CA", "AC", "ACc", "lCA", "R", "lR", "D", "lD",
                                "RD", "lRD"]):
    """
    This function computes the correlation for the data given
    'number_of_simulations' times, using the estimators given
    in estimators as a parameter

    Parameters:
    ----------
    data : list
        data('Y', 'coords' and 'w') of the 4 regions that would be
        used to calculate correlation

    number_of_simulations : int
        the number of times we calculate correlation using all estimators
    estimators  : list of strings
        estimators that would be used to compute correlation

    Returns:
    ----------
    CorrDF : pandas DataFrame
        A data frame containing the estimators and the value of
        the correlation of that estimator

    The number of lines of the data frame is
    number_if_simulations * len(estimators)
    """
    # dataframe to store all estimated corr
    CorrDF = pd.DataFrame(columns=["EST", "CORR"])
    for _ in range(number_of_simulations):
        for estimator in estimators:
            # Loop over correlation estimators
            # Estimate correlation using estCorr function
            corr = estCorr(data, meth=estimator, thresh=0.5)
            # New line to add to the result DataFrame
            newEst = pd.DataFrame({"EST": [estimator], "CORR": [corr["est"]]})
            CorrDF = pd.concat([CorrDF, newEst], ignore_index=True)
    return CorrDF


def process_regions(Y_liste_paths_regions, coords_liste_paths_regions,
                    weights_liste_paths_regions):
    """Gives the data in 4 regions from the paths of the files containing
    these data

    Parameters:
    -----------
    Y_liste_paths_regions : list of strings
        paths to 'Y'

    coords_liste_paths_regions : list of strings
        paths to 'coords'

    weigths_liste_paths_regions : list of strings
        paths to 'w'

    Returns:
    ---------
    data  : list
        The data('Y', 'coords'and 'w') of the 4 regions
    """
    path_region_1, path_region_2, \
        path_region_3, path_region_4 = coords_liste_paths_regions
    coords_region_1, coords_region_2, \
        coords_region_3, coords_region_4 = read(path_region_1, "coords"), \
        read(path_region_2, "coords"), read(path_region_3, "coords"), \
        read(path_region_4, "coords")

    Y_path_region_1, Y_path_region_2, \
        Y_path_region_3, Y_path_region_4 = Y_liste_paths_regions
    Y_region_1, Y_region_2, \
        Y_region_3, Y_region_4 = read(Y_path_region_1), \
        read(Y_path_region_2), read(Y_path_region_3), read(Y_path_region_4)

    weights_path_region_1, weights_path_region_2, \
        weights_path_region_3, \
        weights_path_region_4 = weights_liste_paths_regions
    weights_region_1, weights_region_2,\
        weights_region_3, \
        weights_region_4 = read(weights_path_region_1, "w"), \
        read(weights_path_region_2, "w"), read(weights_path_region_3, "w"), \
        read(weights_path_region_4, "w")

    data = add_to_list_Y_coords_w(
        [Y_region_1, Y_region_2, Y_region_3, Y_region_4],
        [coords_region_1, coords_region_2, coords_region_3, coords_region_4],
        [weights_region_1, weights_region_2, weights_region_3,
         weights_region_4])
    return data


def process_region(Y_path, coords_path, weights_path):
    """Gives the data('Y', 'coords' and 'w') in a region from the paths
    of the files containing these data

    Parameters:
    -----------
    Y_path : string
        path to 'Y'

    coords_path : string
        path to 'coords'

    weigths_path : string
        path to 'w'

    Returns:
    ---------
    data : dict
        The data of the 4 regions ( 'Y', 'coords' and 'w')
    """

    path_region_1 = coords_path
    # extract coords data from the path of the file
    coords_region_1 = read(path_region_1, "coords")

    Y_path_region_1 = Y_path
    # extract Y data from the path of the file
    Y_region_1 = read(Y_path_region_1)

    weights_path_region_1 = weights_path

    # extract w data from the path of the file
    weights_region_1 = read(weights_path_region_1, "w")

    data = {'Y': Y_region_1, 'coords': coords_region_1, 'w': weights_region_1}

    return data
