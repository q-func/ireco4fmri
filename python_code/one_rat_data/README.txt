Files organization in the 'one_rat_data' directory:

"coord_ROI_x.txt": These files contains the coordinates of voxels in the region x

"dwt_nomvt_ts_ROI_x.txt": These files contains the wavelet representation of
time series of voxels in the region x

"ts_ROI_x.txt": These files contains the time series of voxels in the region x

"weight_ROI_x.txt": These files contains the weights associated with voxels in the region x
