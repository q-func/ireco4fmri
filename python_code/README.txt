NB: To know more about what is in a folder, please read
the README.txt file in that folder.

Folders organization in the 'class_of_estimators' directory:

"src": This folder contains the estimators ("CA", "lCA" ...)

"tests": This folder contains tests for estimators in the "src" directory

"benchmarks": This folder contains time benchmarks of estimators

"R_matrices_corr": This folder contains the matrices got from computing
the estimators in R and also the code used to compute them.

"simulations" : This folder contains functions that could be used to simulate
data and how to used them

"python_matrices_of_corr": This folder contains functions to compute correlations
between regions using the data in the folder "one_rat_data"

"one_rat_data": This folder contains the data of the regions of a rat

"Notebooks": This folder contains two Jupyter notebooks illustrating how to use our code.

"Hypothesis_testing": This folder explores how to use the hypothesis library to fuzz test 
our class methods.
