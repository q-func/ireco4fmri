from simulations import build_sim_data
import os
import sys

import numpy as np
import pandas as pd

sys.path.append(os.path.abspath(os.path.join(os.getcwd(), 'src')))
sys.path.append(os.path.abspath(os.path.join(os.pardir, 'src')))

from estCorr import estCorr


np.random.seed(42)
# Simulate two regions
n = 1000  # number of samples
p1 = 20  # number of variables in region 1
p2 = 40  # number of variables in region 2

D = 100  # parameter to control the homogeneity of the region
rmin = 0.6  # intra-correlation parameter

# List of estimators to compute
meth = ["CA", "AC", "ACc", "lCA", "R", "lR", "D", "lD", "RD", "lRD"]

########################
# 2nd setting: no global noise SNReps=10
#######################
vareps = 0.1  # variance of the local noise
vare = 0  # variance of the global noise

# 500 simulations
CorrDF_0e_0pt1eps = pd.DataFrame(
    columns=["EST", "CORR"]
)  # dataframe to store all estimated corr

for i in range(500):
    print(i)
    # Simulate data
    simu = build_sim_data(
        T=n, rmin=rmin, D=D, vareps=vareps, vare=vare, vecn=[p1, p2, 40, 40]
    )
    data = simu["data"]  # Simulated data
    for m in meth:
        # Loop over correlation estimators
        # Estimate correlation using estCorr function (assuming it is defined)
        corr = estCorr(data=data, meth=m, thresh=0)
        newEst = pd.DataFrame(
            {"EST": [m], "CORR": [corr["est"]]}
        )  # New line to add to the result DataFrame

        CorrDF_0e_0pt1eps = pd.concat(
            [CorrDF_0e_0pt1eps, newEst],
            ignore_index=True
        )

# Write the DataFrame to a text file
CorrDF_0e_0pt1eps.to_csv("CorrDF_0e_0pt1eps.txt", index=False)
