import numpy as np
from scipy import linalg


def Sigma(
    matr=None,
    vecn=[20, 80, 40, 40],
    vecv=[1, 2, 1, 1],
    D=500,
    rmin=0.85,
    rinter=0.6
):
    """Auxiliary function to simulate data

    Parameters:
    -----------
    matr : ndarray
        matrix of correlation

    vecn : list of ints
        control the size of the 4 regions

    vecv : list of ints
        control the variance so that the matrix is positive-definite

    D and rmin : int and float
        control the intra correlation ( see function f and toeplitz)

    rinter : float
        correlation of the first 2 regions

    Returns:
    ----------
    matr : ndarray
        matrix Sigma
    """
    # the simulation constraint is 4 regions in 1d
    n1, n2, n3, n4 = vecn[0], vecn[1], vecn[2], vecn[3]
    v1, v2, v3, v4 = vecv[0], vecv[1], vecv[2], vecv[3]

    if matr is None:
        r12, rOth = rinter, 0  # correlation of the first 2 regions.
        matr = np.array(
            [
                [1, r12, rOth, rOth],
                [r12, 1, rOth, rOth],
                [rOth, rOth, 1, rOth],
                [rOth, rOth, rOth, 1],
            ]
        )

    def f(i, vecj, D=D, rmin=0.8):
        jj = []  # Initializing jj with an empty list
        for j in vecj:
            jj.append(1 - min(abs(j - i) / D, 1 - rmin))
        return jj

    list_n1 = [i for i in range(1, n1 + 1)]
    list_n2 = [i for i in range(1, n2 + 1)]
    list_n3 = [i for i in range(1, n3 + 1)]
    list_n4 = [i for i in range(1, n4 + 1)]

    Sigma1 = linalg.toeplitz(f(1, list_n1, D=D, rmin=rmin)) * v1
    Sigma2 = linalg.toeplitz(f(1, list_n2, D=D, rmin=rmin)) * v2
    Sigma3 = linalg.toeplitz(f(1, list_n3, D=D, rmin=rmin)) * v3
    Sigma4 = linalg.toeplitz(f(1, list_n4, D=D, rmin=rmin)) * v4

    Sigma12 = np.full((n1, n2), matr[0, 1]) * np.sqrt(v1 * v2)
    Sigma13 = np.full((n1, n3), matr[0, 2]) * np.sqrt(v1 * v3)
    Sigma14 = np.full((n1, n4), matr[0, 3]) * np.sqrt(v1 * v4)
    Sigma23 = np.full((n2, n3), matr[1, 2]) * np.sqrt(v2 * v3)
    Sigma24 = np.full((n2, n4), matr[1, 3]) * np.sqrt(v2 * v4)
    Sigma34 = np.full((n3, n4), matr[2, 3]) * np.sqrt(v3 * v4)

    Sigma = np.vstack(
        (
            np.hstack(
                (Sigma1, Sigma12, Sigma13, Sigma14)
            ),
            np.hstack(
                (Sigma12.T, Sigma2, Sigma23, Sigma24)
            ),
            np.hstack(
                (Sigma13.T, Sigma23.T, Sigma3, Sigma34)
            ),
            np.hstack(
                (Sigma14.T, Sigma24.T, Sigma34.T, Sigma4)
            ),
        )
    )

    # Calculate eigenvalues and eigenvectors of matrix Sigma
    tmp = np.linalg.eig(Sigma)

    # Check if there is negative eigenvalues
    if np.any(tmp[0] < 0):
        Sigma = None

    matr = {"Sigma": Sigma, "eigenS": tmp}
    return matr


def build_sim_data(
    T=500,
    matr=None,
    vecn=[20, 80, 40, 40],
    vecv=[1, 2, 1, 1],
    D=500,
    rmin=0.85,
    vare=0,
    vareps=0,
    rinter=0.6,
):
    """Simulate data

    Parameters:
    -----------
    T : int
        the number of random samples to generate

    matr : ndarray
        matrix of correlation

    vecn : list of ints
        control the size of the 4 regions

    vecv : list of ints
        control the variance so that the matrix is positive-definite

    D and rmin : int and float
        control the intra correlation ( see function f and toeplitz)

    vare : float
        variance of the global noise

    vareps : float
        variance of the local noise

    rinter : float
        correlation of the first 2 regions

    Returns:
    ---------
    dict_data : dict
        a dictionnary containing simulated data
    """
    tmpS = Sigma(matr=matr,
                 vecn=vecn,
                 vecv=vecv,
                 D=D,
                 rmin=rmin,
                 rinter=rinter)

    if tmpS["eigenS"] is None:
        return "simulation not available, some eigenvalues are negative"

    S = tmpS["Sigma"]

    YY = np.random.multivariate_normal(
        mean=np.zeros(S.shape[0]),
        cov=S,
        size=T
    )

    e = np.random.normal(0, np.sqrt(vare), T)
    n0, n1, n2, n3 = vecn[0], vecn[1], vecn[2], vecn[3]

    data = []
    for i in range(4):
        if i == 0:
            ind = np.array(list(range(1, n0 + 1)))
        elif i == 1:
            ind = np.array(list(range(n0 + 1, n0 + n1 + 1)))
        elif i == 2:
            ind = np.array(list(range(n0 + n1 + 1, n0 + n1 + n2 + 1)))
        elif i == 3:
            ind = np.array(list(
                range(n0 + n1 + n2 + 1, n0 + n1 + n2 + n3 + 1)
                    )
                )
        indice_acces = ind - 1
        data.append(
            {
                "Y": YY[:, indice_acces]
                + e[:, np.newaxis]
                + np.random.multivariate_normal(
                    np.zeros(len(ind)), np.diag(ind) * vareps, size=T
                ),
                "coords": np.vstack(
                    (
                        indice_acces,
                        np.zeros_like(indice_acces),
                        np.zeros_like(indice_acces),
                    )
                ),
                "w": np.array([1] * len(ind)),
            }
        )
    dict_data = {"data": data, "S": S}
    return dict_data


def rowMeansw(Y, w):
    """Calculate the weighted mean of each row in matrix Y,
    using the specified weights in vector w.

    Parameters:
    ------------
    Y : ndarray
        a matrix where each row represents an observation

    w : ndarray
        a weight vector of the same length as the number of columns in Y

    Returns:
    A vector containing the weighted means of each row in Y
    """
    return np.apply_along_axis(lambda x: np.average(x, weights=w),
                               axis=1,
                               arr=Y)
