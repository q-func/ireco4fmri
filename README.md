This is the repository for the paper 'Inter-regional correlation estimators for functional magnetic resonance imaging'.

It contains data and code to reproduce the results in the paper.

# Organisation
- `Output`: correlation estimates for the simulation, rat, and human data
- `Paper_code`: code for the figures in the paper (Rmarkdown)
- `R_code`: R package implementing correlation estimators.

# Installation

The package can be installed with `install.packages('./ireco4fmri/R_code/',repos=NULL,type='source')`


# Data

## Rat names
- rat **20160524_153000**: dead rat
- rat **20160616_103405**: live rat (Eto-L)
- rat **20160615_103000**: live rat (Iso-W)

## Output files description

### Edge_list_Rat_corrfmripaper.txt 
File listing the pairs of regions in the order in which the inter-correlations were estimated. Each column corresponds to a pair of regions. 

### Rat_XXX_Bigregs_CorrEstimates.txt
File storing the correlations computed on rat XXX using the following estimators: "agg", "ave", "aggc", "laveB", "raveB", "lraveB", "saveB", "slaveB", "sraveB", "raveB". There are two columns:

- **EST**: the name of the estimator,

- **CORR**: the corresponding correlation coefficient.

**Remark**: "agg"="CA", "ave"="AC", "raveB"="R", "laveB"="lCA", "lraveB"="lR","saveB"="D", "sraveB"="RD", "slaveB"="lD", "slraveB"="lRD"

### Rat_XXX_Bigregs_tscrop1_CorrEstimates.txt
Same type of file as *Rat_XXX_Bigregs_CorrEstimates.txt*. It corresponds to the correlations estimated on the first half of the time series. The correlations estimated on the second half of the time series are called: *Rat_XXX_Bigregs_tscrop2_CorrEstimates.txt*.


## Directory organization
For the **HCP** data:
<pre>
HCP_data
	sub-XXX
		est-AAA
			ses-0
				sub_XXX-est_AAA-ses_0.csv
			ses-1
				sub_XXX-est_AAA-ses_1.csv
		est-BBB
			...
	sub-EEE
		...
</pre>
where the *sub-XXX* folders corresponds to subject XXX, *est-AAA* to correlation estimator AAA, *ses-0* and *ses-1* to exam0 and exam1 of subject XXX.


Similarly, for the **rat** data:
<pre>
Rat_data
	sub-XXX
		est-AAA
			tscrop-1
				sub_XXX-est_AAA-tscrop_1.csv
			tscrop-2
				sub_XXX-est_AAA-tscrop-2.csv
			tsfull
				sub_XXX-est_AAA-tsfull.csv
		est-BBB
			...
	sub-EEE
		...
</pre>

where the *tscrop_1* and *tscrop_2* corresponds to the first and second halves of the time series, respectively, and *tsfull* to the full time series.

The *.csv files contain the correlation matrix of the corresponding subject, session and estimator.
