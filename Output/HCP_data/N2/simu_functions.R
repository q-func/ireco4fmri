
################################ DATA SIMULATION FUNCTIONS #######################################
##M<-Sigma(vecn=c(10,10,2,2),vecv=c(1,4,2,2),D=500,rmin=0.85)
### vecn = controle de la taille des 4 regions
## vecv = controle de la variance pour que la matrice soit definie positive 
## D et rmin = controle de la correlation intra, voir fonction f et la matrice est Toeplitz 

Sigma<-function(matr=NULL,vecn=c(20,80,40,40),vecv=c(1,2,1,1),D=500,rmin=.85, rinter=.6){
  ## la contrainte de la simulation c'est 4 régions en 1d
  n1<-vecn[1];n2<-vecn[2];n3<-vecn[3];n4<-vecn[4]
  v1<-vecv[1];v2<-vecv[2];v3<-vecv[3];v4<-vecv[4]
  if (is.null(matr)){
    r12<-rinter;rOth<-0 ### correlation des 2 premieres regions. 
    matr<-cbind(c(1,r12,rOth,rOth),c(r12,1,rOth,rOth),c(rOth,rOth,1,rOth),c(rOth,rOth,rOth,1))
    
  }
  #print(matr)
  f<-function(i,vecj,D=D,rmin=.8) {
    jj<-NULL
    for (j in vecj) jj<-c(jj,1-min(abs(j-i)/D,1-rmin)) #c(jj,rmin)
    jj
  }
  
  
  Sigma1<-toeplitz(f(1,1:n1,D=D,rmin=rmin))*v1
  Sigma2<-toeplitz(f(1,1:n2,D=D,rmin=rmin))*v2
  Sigma3<-toeplitz(f(1,1:n3,D=D,rmin=rmin))*v3
  Sigma4<-toeplitz(f(1,1:n4,D=D,rmin=rmin))*v4
  Sigma12<-matrix(matr[1,2],nr=n1,nc=n2)*sqrt(v1*v2)
  Sigma13<-matrix(matr[1,3],nr=n1,nc=n3)*sqrt(v1*v3)
  Sigma14<-matrix(matr[1,4],nr=n1,nc=n4)*sqrt(v1*v4)
  Sigma23<-matrix(matr[2,3],nr=n2,nc=n3)*sqrt(v2*v3)
  Sigma24<-matrix(matr[2,4],nr=n2,nc=n4)*sqrt(v2*v4)
  Sigma34<-matrix(matr[3,4],nr=n3,nc=n4)*sqrt(v3*v4)
  
  Sigma<-rbind(cbind(Sigma1,Sigma12,Sigma13,Sigma14),cbind(t(Sigma12),
                                                           Sigma2,Sigma23,Sigma24),cbind(t(Sigma13),t(Sigma23),Sigma3,Sigma34),
               cbind(t(Sigma14),t(Sigma24),t(Sigma34),Sigma4) )
  tmp<-eigen(Sigma)
  if (sum((tmp$values)<0)>0) Sigma<-eigenS<-NULL
  list(Sigma=Sigma,eigenS=tmp)
}

buildSimData<-function(T=500,matr=NULL,vecn=c(20,80,40,40),vecv=c(1,2,1,1),D=500,rmin=.85,vare=0,vareps=0, rinter=.6){
  require(mvtnorm)
  Sigma(matr=matr,vecn=vecn,vecv=vecv,D=D,rmin=rmin, rinter=rinter)->tmpS
  if (is.null(tmpS)) return('simulation not available, some eigenvalues are negative')
  S<-tmpS$Sigma
  YY<- rmvnorm(n=T,sigma=S)
  data<-list()
  e<-rnorm(T,0,sd=sqrt(vare))
  n1<-vecn[1];n2<-vecn[2];n3<-vecn[3];n4<-vecn[4]
  for (i in 1:4){
    ind<-switch(i,
                "1"={1:n1},"2"={(n1+1):(n1+n2)},"3"={(n1+n2+1):(n1+n2+n3)},"4"={(n1+n2+n3+1):(n1+n2+n3+n4)}
    )
    data[[i]]<-list()
    data[[i]]$Y<-YY[,ind]+e +rmvnorm(n=T,sig=diag(ind)*vareps)
    data[[i]]$region<-rbind(ind,1,1)
    data[[i]]$w<-rep(1,length(ind))
  }
  list(data=data, S=S)
}

rowMeansw<-function(Y,w){
  apply(Y,1,weighted.mean,w=w)
}



########################### CORRELATION ESTIMATOR FUNCTION #########################################
estCorr<-function(data,meth=c("agg","ave","aggc","laveB","raveB","lraveB","saveB","slaveB","sraveB","slraveB"),
                  param=list(B=500,dNeigh=1,dRepl=1),thresh=.5){
  ## Conventions: par région une matrice de données Yj de dimension T* #Rj  
  ##				liste des coordonnées spatiales de chaque voxel définissant de Yj de dim. 3*#Rj
  ## data= liste de dimension 4, les deux premiers éléments sont les régions dont on veut calculer la corrélation
  ## 		 les deux autres sont a priori décorrélées des deux premières. 
  ##		 donc on estime que r12
  nbReg<-length(data)
  
  i1<-which(data[[1]]$w>thresh)
  i2<-which(data[[2]]$w>thresh)
  i3<-which(data[[3]]$w>thresh)
  i4<-which(data[[4]]$w>thresh)
  
  Y1<-data[[1]]$Y[,i1];reg1<-data[[1]]$region[,i1]
  Y2<-data[[2]]$Y[,i2];reg2<-data[[2]]$region[,i2]
  Y3<-data[[3]]$Y[,i3];reg3<-data[[3]]$region[,i3]
  Y4<-data[[4]]$Y[,i4];reg4<-data[[4]]$region[,i4]
  
  d1<-as.matrix(dist(t(reg1),meth="maximum"))
  d2<-as.matrix(dist(t(reg2),meth="maximum"))
  d3<-as.matrix(dist(t(reg3),meth="maximum"))
  d4<-as.matrix(dist(t(reg4),meth="maximum"))
  
  dNeigh<-param$dNeigh
  dRepl<-param$dRepl
  B<-param$B
  
  
  corNoisy<-function(Y1,Y2,Y3,Y4){
    ## appliquable pour Y1,Y2,Y3,Y4 matrices
    v1<-diag( as.matrix( (var(Y1-Y3)+var(Y1-Y4)-var(Y3-Y4))/2  ))
    v2<-diag( as.matrix( (var(Y2-Y3)+var(Y2-Y4)-var(Y3-Y4))/2  ))
    if (any(c(v1,v2)<0)) return(NA)
    v<-v1%*%t(v2)
    c(cov(Y1-Y3,Y2-Y4)/sqrt(v) )
    
  }
  
  corLocNoisy<-function(Y1,Y1repl,Y2,Y2repl){
    c1<-cor(Y1,Y1repl);c2<-cor(Y2,Y2repl)
    if (any(c(c1,c2) <0)) return(NA)
    else { mean(cor(cbind(Y1,Y1repl),cbind(Y2,Y2repl)))/sqrt(c1*c2 )}
  }
  
  #corDoubleNoise2<-function(Y1,Y1repl,Y2,Y2repl,Y3,Y3repl,Y4,Y4repl){
  
  #			num<-mean(corNoisy(cbind(Y1,Y1repl),cbind(Y2,Y2repl), cbind(Y3,Y3repl),cbind(Y4,Y4repl) ))
  #			if (any(c(corNoisy(Y1,Y1repl,Y3,Y3repl),corNoisy(Y2,Y2repl,Y4,Y4repl),2) <0)) return(NA)
  #			denom<- sqrt( corNoisy(Y1,Y1repl,Y3,Y3repl)*corNoisy(Y2,Y2repl,Y4,Y4repl) )
  #			num/denom
  #}
  
  corDoubleNoise<-function(Y1,Y1repl,Y2,Y2repl,Y3,Y4){
    num<-mean(corNoisy(cbind(Y1,Y1repl),cbind(Y2,Y2repl), cbind(Y3,Y3),cbind(Y4,Y4) ))
    c1<-corNoisy(Y1,Y1repl,Y3,Y4);c2<-corNoisy(Y2,Y2repl,Y3,Y4)
    if (sum(is.na(c(c1,c2)))>0) return(NA)
    if (any(c(c1,c2)<0)) return(NA)
    denom<- sqrt( c1*c2 )
    num/denom
  }
  
  outside<-function(est){
    ifelse((est<(-1))|(est>1), NA, est)
  }
  
  sumNA<-onlyOne<-0
  
  switch(meth,
         "agg"={
           est<-max(min(cor(rowMeans(Y1),rowMeans(Y2)),1),-1)
         },
         "ave"={
           est<-max(min(mean(cor(Y1,Y2)),1),-1)
         },
         "aggc"={
           est<-max(min(sqrt(mean(cor(Y1,Y1)) * mean(cor(Y2,Y2))) * cor(rowMeans(Y1),rowMeans(Y2)),1),-1)
         },
         "laveB"={
           res<-rep(0,B)
           for (b in 1:B){
             i1<-sample(1:ncol(Y1),1)
             neigh1<-which(d1[i1,]<=dNeigh)
             Y1b<-rowMeans(as.matrix(Y1[,neigh1]))
             i2<-sample(1:ncol(Y2),1)
             neigh2<-which(d2[i2,]<=dNeigh)
             Y2b<-rowMeans(as.matrix(Y2[,neigh2]))
             res[b]<-cor(Y1b,Y2b)
             if (  (length(neigh1)==1) | (length(neigh2)==1)) onlyOne<-onlyOne+1
           }
           est<-max(min(mean(res),1),-1)
         },
         "raveB"={
           res<-rep(0,B)
           for (b in 1:B){
             
             i1<-sample(1:ncol(Y1),1)
             i2<-sample(1:ncol(Y2),1)
             while ( (sum(d1[i1,]==dRepl)==0) | (sum(d2[i2,]==dRepl)==0) ) {
               onlyOne<-onlyOne+1
               i1<-sample(1:ncol(Y1),1)
               i2<-sample(1:ncol(Y2),1)
             }
             repli1<-sample(which(d1[i1,]==dRepl),1)
             repli2<-sample(which(d2[i2,]==dRepl),1)
             Y1b<-Y1[,i1];Y1replb<-Y1[,repli1]
             Y2b<-Y2[,i2];Y2replb<-Y2[,repli2]
             res[b]<-outside(corLocNoisy(Y1b,Y1replb,Y2b,Y2replb))
             
           }
           ##browser()
           est<-max(min(mean(res,na.rm=TRUE),1),-1)
           sumNA<-sum(is.na(res))
         },
         "lraveB"={
           res<-rep(0,B)
           for (b in 1:B){
             i1<-sample(1:ncol(Y1),1)
             neigh1<-which(d1[i1,]<=dNeigh)
             Y1b<-rowMeans(as.matrix(Y1[,neigh1]))
             repli1<-sample(which(d1[i1,]==(dRepl+2*dNeigh)) ,1)
             neighRepl1<-which(d1[repli1,]<=dNeigh)
             Y1replb<-rowMeans(as.matrix(Y1[,neighRepl1]))
             i2<-sample(1:ncol(Y2),1)
             neigh2<-which(d2[i2,]<=dNeigh)
             Y2b<-rowMeans(as.matrix(Y2[,neigh2]))
             repli2<-sample(which(d2[i2,]==(dRepl+2*dNeigh)) ,1)
             neighRepl2<-which(d2[repli2,]<=dNeigh)
             Y2replb<-rowMeans(as.matrix(Y2[,neighRepl2]))
             res[b]<-outside(corLocNoisy(Y1b,Y1replb,Y2b,Y2replb))
             if (  (length(neigh1)==1) | (length(neigh2)==1)|
                   (length(neighRepl1)==1)|(length(neighRepl2)==1) ) onlyOne<-onlyOne+1
           }
           est<-max(min(mean(res,na.rm=TRUE),1),-1)
           sumNA<-sum(is.na(res))
         },
         "saveB"={
           res<-rep(0,B)
           for (b in 1:B){
             i1<-sample(1:ncol(Y1),1)
             i3<-sample(1:ncol(Y3),1)
             i2<-sample(1:ncol(Y2),1)
             i4<-sample(1:ncol(Y4),1)
             Y1b<-Y1[,i1];Y3b<-Y3[,i3];Y2b<-Y2[,i2];Y4b<-Y4[,i4]
             res[b]<-outside(corNoisy(Y1b,Y2b,Y3b,Y4b))
           }
           est<-max(min(mean(res,na.rm=TRUE),1),-1)
           sumNA<-sum(is.na(res))
         },
         "slaveB"={
           res<-rep(0,B)
           for (b in 1:B){
             i1<-sample(1:ncol(Y1),1)
             neigh1<-which(d1[i1,]<=dNeigh)
             Y1b<-rowMeans(as.matrix(Y1[,neigh1]))
             i2<-sample(1:ncol(Y2),1)
             neigh2<-which(d2[i2,]<=dNeigh)
             Y2b<-rowMeans(as.matrix(Y2[,neigh2]))
             i3<-sample(1:ncol(Y3),1)
             neigh3<-which(d3[i3,]<=dNeigh)
             Y3b<-rowMeans(as.matrix(Y3[,neigh3]))
             i4<-sample(1:ncol(Y4),1)
             neigh4<-which(d4[i4,]<=dNeigh)
             Y4b<-rowMeans(as.matrix(Y4[,neigh4]))
             res[b]<-outside(corNoisy(Y1b,Y2b,Y3b,Y4b))
             if ( (length(neigh1)==1) | (length(neigh2)==1)|
                  (length(neigh3)==1)|(length(neigh4)==1) ) onlyOne<-onlyOne+1
             #browser()
           }
           est<-max(min(mean(res,na.rm=TRUE),1),-1)
           suNA<-sum(is.na(res))
         },
         "sraveB"={
           res<-rep(0,B)
           #set.seed(123)
           for (b in 1:B){
             i1<-sample(1:ncol(Y1),1)
             i2<-sample(1:ncol(Y2),1)
             while ( (sum(d1[i1,]==dRepl)==0) | (sum(d2[i2,]==dRepl)==0) ) {
               onlyOne<-onlyOne+1
               i1<-sample(1:ncol(Y1),1)
               i2<-sample(1:ncol(Y2),1)
             }
             repli1<-sample(which(d1[i1,]==dRepl),1)
             repli2<-sample(which(d2[i2,]==dRepl),1)
             i3<-sample(1:ncol(Y3),1)
             i4<-sample(1:ncol(Y4),1)
             Y1b<-Y1[,i1];Y1replb<-Y1[,repli1]
             Y2b<-Y2[,i2];Y2replb<-Y2[,repli2]
             Y3b<-Y3[,i3]
             Y4b<-Y4[,i4]
             res[b]<-outside(corDoubleNoise(Y1b,Y1replb,Y2b,Y2replb,Y3b,Y4b))
           }
           est<-max(min(mean(res,na.rm=TRUE),1),-1)
           sumNA<-sum(is.na(res))
         },
         "slraveB"={
           res<-rep(0,B)
           for (b in 1:B){
             i1<-sample(1:ncol(Y1),1)
             neigh1<-which(d1[i1,]<=dNeigh)
             Y1b<-rowMeans(as.matrix(Y1[,neigh1]))
             repli1<-sample(which(d1[i1,]==(dRepl+2*dNeigh)) ,1)
             neighRepl1<-which(d1[repli1,]<=dNeigh)
             Y1replb<-rowMeans(as.matrix(Y1[,neighRepl1]))
             i2<-sample(1:ncol(Y2),1)
             neigh2<-which(d2[i2,]<=dNeigh)
             Y2b<-rowMeans(as.matrix(Y2[,neigh2]))
             repli2<-sample(which(d2[i2,]==(dRepl+2*dNeigh)) ,1)
             neighRepl2<-which(d2[repli2,]<=dNeigh)
             Y2replb<-rowMeans(as.matrix(Y2[,neighRepl2]))
             i3<-sample(1:ncol(Y3),1)
             neigh3<-which(d3[i3,]<=dNeigh)
             Y3b<-rowMeans(as.matrix(Y3[,neigh3]))
             i4<-sample(1:ncol(Y4),1)
             neigh4<-which(d4[i4,]<=dNeigh)
             Y4b<-rowMeans(as.matrix(Y4[,neigh4]))
             
             
             res[b]<-outside(corDoubleNoise(Y1b,Y1replb,Y2b,Y2replb,Y3b,Y4b))
             if ( (length(neigh1)==1) | (length(neigh2)==1)|(length(neighRepl1)==1)
                  |(length(neighRepl2)==1) |(length(neigh3)==1)|(length(neigh4)==1)  ) onlyOne<-onlyOne+1
             
           }
           est<-max(min(mean(res,na.rm=TRUE),1),-1)
           sumNA<-sum(is.na(res))
         })
  
  
  list(est=est,sumNA=sumNA,onlyOne=onlyOne)
}

testAll<-function(data,meth=c("agg","ave","aggc","laveB","raveB","lraveB","saveB","slaveB","sraveB","slraveB"),
                  param=list(B=500,dNeigh=1,dRepl=1),thresh=.5){
  
  for (m in meth){
    cat('----------------- Method:',m,'\n')
    print(estCorr(data=data,meth=m,param=param,thresh=thresh))
  }
}

#######################################
set.seed(42)

####### simulate two regions
n <- 1000 # number of samples
p1 <- 20 # number of variables in region 1
p2 <- 40 # number of variables in region 2

D <- 100 # parameter to control the homogeneity of the region
rmin <- .6 # intra-correlation parameter 

## list of estimators to compute
meth <- c("agg","ave","laveB","raveB","lraveB","saveB","slaveB","sraveB","slraveB","aggc")

#########################
## 1st setting: no noise
########################
vareps <- 0 # variance of the noise
vare <- 0 # variance of the global noise

#### 500 simulations
CorrDF_0e_0eps <- data.frame(stringsAsFactors=FALSE) # dataframe to store all estimated corr
for(i in 1:500){
  cat(i, "\n")
  ## simulate data
  simu <- buildSimData( T=n,rmin=rmin, D=D, vareps=vareps, vare = vare, vecn=c(p1,p2,40,40))
  data <- simu$data # simulated data
  
  for (m in meth){
    # loop over correlation estimators
    corr <- estCorr(data=data,meth=m,thresh=0) # estimated correlation
    newEst <- data.frame(EST=m, CORR=corr$est) # new line to add to the result dataframe
    CorrDF_0e_0eps <- rbind(CorrDF_0e_0eps, newEst)
  }
}
write.table(CorrDF_0e_0eps, "CorrDF_0e_0eps.txt", row.names = F)


#########################
## 2nd setting: no global noise SNReps=10
########################
vareps <- 0.1 # variance of the local noise
vare <- 0 # variance of the global noise

#### 500 simulations
CorrDF_0e_0pt1eps <- data.frame(stringsAsFactors=FALSE) # dataframe to store all estimated corr
for(i in 1:500){
  cat(i, "\n")
  ## simulate data
  simu <- buildSimData( T=n,rmin=rmin, D=D, vareps=vareps, vare = vare, vecn=c(p1,p2,40,40))
  data <- simu$data # simulated data
  
  for (m in meth){
    # loop over correlation estimators
    corr <- estCorr(data=data,meth=m,thresh=0) # estimated correlation
    newEst <- data.frame(EST=m, CORR=corr$est) # new line to add to the result dataframe
    CorrDF_0e_0pt1eps <- rbind(CorrDF_0e_0pt1eps, newEst)
  }
}
write.table(CorrDF_0e_0pt1eps, "CorrDF_0e_0pt1eps.txt", row.names = F)


#########################
## 3rd setting: no local noise SNRe=10
########################
vareps <- 0 # variance of the local noise
vare <- 0.1 # variance of the global noise

#### 500 simulations
CorrDF_0pt1e_0eps <- data.frame(stringsAsFactors=FALSE) # dataframe to store all estimated corr
for(i in 1:500){
  cat(i, "\n")
  ## simulate data
  simu <- buildSimData( T=n,rmin=rmin, D=D, vareps=vareps, vare = vare, vecn=c(p1,p2,40,40))
  data <- simu$data # simulated data
  
  for (m in meth){
    # loop over correlation estimators
    corr <- estCorr(data=data,meth=m,thresh=0) # estimated correlation
    newEst <- data.frame(EST=m, CORR=corr$est) # new line to add to the result dataframe
    CorrDF_0pt1e_0eps <- rbind(CorrDF_0pt1e_0eps, newEst)
  }
}
write.table(CorrDF_0pt1e_0eps, "CorrDF_0pt1e_0eps.txt", row.names = F)


#########################
## 4th setting: SNRe=10 and SNReps=10
########################
vareps <- 0.1 # variance of the local noise
vare <- 0.1 # variance of the global noise

#### 500 simulations
CorrDF_0pt1e_0pt1eps <- data.frame(stringsAsFactors=FALSE) # dataframe to store all estimated corr
for(i in 1:500){
  cat(i, "\n")
  ## simulate data
  simu <- buildSimData( T=n,rmin=rmin, D=D, vareps=vareps, vare = vare, vecn=c(p1,p2,40,40))
  data <- simu$data # simulated data
  
  for (m in meth){
    # loop over correlation estimators
    corr <- estCorr(data=data,meth=m,thresh=0) # estimated correlation
    newEst <- data.frame(EST=m, CORR=corr$est) # new line to add to the result dataframe
    CorrDF_0pt1e_0pt1eps <- rbind(CorrDF_0pt1e_0pt1eps, newEst)
  }
}
write.table(CorrDF_0pt1e_0pt1eps, "CorrDF_0pt1e_0pt1eps.txt", row.names = F)

#########################
## 5th setting: SNRe=0 and SNReps=20
########################
vareps <- 0.1 # variance of the local noise
vare <- 0. # variance of the global noise

#### 500 simulations
CorrDF_0pt1e_0pt001eps <- data.frame(stringsAsFactors=FALSE) # dataframe to store all estimated corr
for(i in 1:10){
  cat(i, "\n")
  ## simulate data
  simu <- buildSimData( T=n,rmin=rmin, D=D, vareps=vareps, vare = vare, vecn=c(400,1600,40,40),vecv=c(1,5,2,7))
  data <- simu$data # simulated data
  
  for (m in meth){
    # loop over correlation estimators
    corr <- estCorr(data=data,meth=m,thresh=0) # estimated correlation
    newEst <- data.frame(EST=m, CORR=corr$est) # new line to add to the result dataframe
    CorrDF_0pt1e_0pt001eps <- rbind(CorrDF_0pt1e_0pt001eps, newEst)
  }
}
write.table(CorrDF_0pt1e_0pt001eps, "CorrDF_0pt1e_0pt001eps.txt", row.names = F)
